import { Injectable } from '@angular/core';

/**
 * Some simple config values
 */
@Injectable()
export class ConfigService {
  public url(path:string):string {
    return 'http://localhost:8080/' + path;
  }
}
