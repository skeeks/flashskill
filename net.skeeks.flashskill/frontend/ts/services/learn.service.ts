import { Injectable } from '@angular/core';
import { Server } from './server.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Response, Headers } from '@angular/http';

/**
 * services access learning operations on the server
 */
@Injectable()
export class LearnService {
    // injected by angular DI
    constructor(private server: Server) { }

    getCurrentCard(cardfileid: string) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.get('sapi/learn?cardfileid=' + cardfileid, {}).subscribe(
                (response: Response) => { observer.next(response.json()); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    processGuess(cardfileid: string, guess: string) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/learn', { "id": cardfileid, "guess": guess }).subscribe(
                (response: Response) => { observer.next(response.json()); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }
}

export interface Card {
    term: string;
    definition: string;
    missedCount?: number;
    knownCount?: number;
    section: number;
}
