import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

import { Server } from './server.service';

import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Response, Headers } from '@angular/http';

/**
 * Login service responsible for logging in users
 */
@Injectable()
export class LoginService {
    private JWT_LOCAL_STORAGE_KEY: string = "FLASHSKILL_JWT";
    private JWT_SERVER_RESPONSE_HEADER: string = "X-JWT";
    private JWT_SERVER_REQUEST_HEADER: string = "X-JWT";

    // Observable string sources
    private loginState = new Subject<LoginState>();
    // Observable string streams
    loginState$ = this.loginState.asObservable();
    // injected by angular DI
    constructor(private server: Server, private authService: AuthService) {
        this.loginState.next(LoginState.UNAUTHORIZED);
    }

    login(username: string, password: string): Observable<any> {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('api/login', {
                'username': username,
                'password': password
            }).subscribe(
                (response: Response) => {
                    observer.next(LoginState[response.json().message]);
                    observer.complete();
                    that.changeLoginState(response.json().message);
                },
                (response: Response) => {
                    observer.error(response.json().message);
                    that.changeLoginState("UNAUTHORIZED");
                }
                );
        });
    }

    logout() {
        let that = this;
        return Observable.create(function(observer: any) {
            that.server.post('sapi/logout', {}).subscribe(
                (response: Response) => {
                    observer.next(LoginState.UNAUTHORIZED);
                    observer.complete();
                    that.changeLoginState("UNAUTHORIZED");
                    that.authService.clearJWT();
                },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    checkLoginState() {
        let that = this;
        return Observable.create(function(observer: any) {
            that.server.get('api/authstate', {}).subscribe(
                (response: Response) => {
                    that.changeLoginState(response.json().message);
                    observer.next(LoginState[response.json().message]);
                    observer.complete()
                },
                (response: Response) => {
                    that.changeLoginState('UNAUTHORIZED');
                    observer.error(response.json().message);
                }
            );
        });
    }

    // Service message commands
    changeLoginState(state: string) {
        this.loginState.next(LoginState[state]);
    }
}

export enum LoginState {
    UNAUTHORIZED, USER, ADMIN
}
