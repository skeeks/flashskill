import { Injectable } from '@angular/core';

export class Card {
	id:string;
	term:string;
	definition:string;
	knownCount:number;
	missedCount:number;
	section:number;
}

/**
 * not used
 */
@Injectable()
export class CardService {

}
