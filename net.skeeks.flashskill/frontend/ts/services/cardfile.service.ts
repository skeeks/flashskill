import { Injectable } from '@angular/core';
import { Card } from './card.service.ts';
import { Server } from './server.service';
import { Observable } from 'rxjs/Observable';
import { Response, Headers } from '@angular/http';
import { Observer } from 'rxjs/Observer';

/**
 * Operations on CardFile's
 *
 */
@Injectable()
export class CardFileService {

    //injected by angular DI
    constructor(private server: Server) { }

    /**
     * get all card files from server
     */
    getCardFiles(): Observable<any> {
        let that = this;
        // we work with obersable, which can be subscribed.
        return Observable.create(function(observer: Observer<any>) {
            that.server.get('sapi/cardfiles', {}).subscribe(
                (response: Response) => { observer.next(response.json().cardFiles); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    /**
     * store card file to server
     */
    storeCardFile(cardFile: CardFile): Observable<any> {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/cardfile/store', cardFile).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    /**
     * reset card file
     */
    resetCardFile(cardFile: CardFile): Observable<any> {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/cardfile/reset', { id: cardFile.id }).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    /**
     * delete card File
     */
    deleteCardFile(cardFile: CardFile): Observable<any> {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/cardfile/delete', { id: cardFile.id }).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    getCardFile(id: string): Observable<any> {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.get('sapi/cardfile/load?cardfileid=' + id, {}).subscribe(
                (response: Response) => { observer.next(response.json()); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    loadStatistics(id: string): Observable<StatisticsModel> {
        let that = this;
        return Observable.create(function(observer: Observer<StatisticsModel>) {
            that.server.get('/sapi/cardfile/statistics?cardfileid=' + id, {}).subscribe(
                (response: Response) => { observer.next(response.json()); observer.complete() },
                (response: Response) => { observer.error(response.json().message); }
            )
        });
    }
}
export class StatisticsModel {
    cardfilename: string;
    topMissedCards: Card[];
    topKnownCards: Card[];
    sections: any;
}

export class CardFile {
    id: string;
    name: string;
    cards: Card[];
    lastStudyDate: string;
}
