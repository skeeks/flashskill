import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';

/**
 * responsible for adding JWT to server requests and also for refreshing JWT on each request.
 */
@Injectable()
export class AuthService {
    private JWT_LOCAL_STORAGE_KEY: string = "FLASHSKILL_JWT";
    private JWT_SERVER_RESPONSE_HEADER: string = "X-JWT";
    private JWT_SERVER_REQUEST_HEADER: string = "X-JWT";

    constructor() { }

    public refreshJWT(response: Response): void {
        if (response && response.headers && response.headers.get(this.JWT_SERVER_RESPONSE_HEADER)) {
            window.localStorage.setItem(this.JWT_LOCAL_STORAGE_KEY, response.headers.get(this.JWT_SERVER_RESPONSE_HEADER));
        }
    }

    public addAuthorizationHeader(headers: Headers): Headers {
        if (!window.localStorage) {
            alert("Dein Browser unterstützt die Local-Storage Funktion nicht. Diese Funktion wird jedoch von Flashskill benötigt.");
        }
        let jwt: string = window.localStorage.getItem(this.JWT_LOCAL_STORAGE_KEY);
        if (jwt) {
            headers.append(this.JWT_SERVER_REQUEST_HEADER, window.localStorage.getItem(this.JWT_LOCAL_STORAGE_KEY));
        }
        return headers;
    }

    public clearJWT() {
        window.localStorage.removeItem(this.JWT_LOCAL_STORAGE_KEY);
    }

}
