import { Injectable } from '@angular/core';
import { Server } from './server.service';
import { Observable } from 'rxjs/Observable';
import { Response, Headers } from '@angular/http';
import { Observer } from 'rxjs/Observer';

/**
 * Operations to change user
 */
@Injectable()
export class UserService {

    // injected by angular DI
    constructor(private server: Server) { }

    getUsers() {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.get('sapi/users', {}).subscribe(
                (response: Response) => { observer.next(response.json().users); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    storeUser(user: User) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/user/store', user).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    getUser(id: string) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.get('sapi/user/load?userid=' + id, {}).subscribe(
                (response: Response) => { observer.next(response.json()); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    deleteUser(user: User) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/user/delete', { id: user.id }).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    registerUser(username: string, password: string) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('api/register', { 'username': username, 'password': password }).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }

    changeOwnPassword(newPassword: string, oldPassword: string) {
        let that = this;
        return Observable.create(function(observer: Observer<any>) {
            that.server.post('sapi/user/changepassword', { 'newPassword': newPassword, 'oldPassword': oldPassword }).subscribe(
                (response: Response) => { observer.next(response.json().message); observer.complete() },
                (response: Response) => observer.error(response.json().message)
            );
        });
    }
}

export class User {
    id: string;
    username: string;
    admin: boolean;
    lastLoginDate: string;
}
