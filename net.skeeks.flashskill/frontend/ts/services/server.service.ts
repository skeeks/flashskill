import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Router} from '@angular/router-deprecated';

/**
 * Service for all Server calls
 */
@Injectable()
export class Server {
    // injeted by angular Di
    constructor(private config: ConfigService, private auth: AuthService, private http: Http, private router: Router) { }

    public get(path: string, data: Object): Observable<any> {
        var that = this;
        return this.http.get(this.execBuildUrl(path), this.execBuildRequestOptions())
            .map(resp => {
                that.auth.refreshJWT(resp);
                return resp;
            })
            .catch(this.execHandleError.bind(that));
    }

    public post(path: string, data: Object): Observable<any> {
        var that = this;
        return this.http.post(this.execBuildUrl(path), this.execBuildBody(data), this.execBuildRequestOptions())
            .map(resp => {
                that.auth.refreshJWT(resp);
                return resp;
            })
            .catch(this.execHandleError.bind(that));
    }

    protected execBuildUrl(path: string): string {
        return this.config.url(path);
    }

    protected execBuildRequestOptions(): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.auth.addAuthorizationHeader(headers);
        return new RequestOptions({ 'headers': headers });
    }

    protected execBuildBody(data: Object): string {
        return JSON.stringify(data);
    }

    private execHandleError(response: Response) {
        if (response.status == 200) {
            this.router.navigate(['ErrorPage', { message: "Der Server konnte nicht erreicht werden." }]);
        }
        if (response.status == 405) {
            this.router.navigate(['ErrorPage', { message: "Der Server hat ein unerwartetes Ergebnis zurückgeliefert." }]);
        }
        return Observable.throw(response);
    }
}
