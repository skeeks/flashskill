import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent } from './components/app.component';
import { HTTP_PROVIDERS } from '@angular/http';
import { ConfigService } from './services/config.service';
import { AuthService } from './services/auth.service';
import { Server } from './services/server.service';
import { ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { disableDeprecatedForms, provideForms } from '@angular/forms';

/*
 * Entry Point for the application
 */
if (process.env.ENV === 'production') {
    enableProdMode();
}

// bootstrap angular, AppComponent as top Component
bootstrap(AppComponent, [HTTP_PROVIDERS, ROUTER_PROVIDERS, AuthService, ConfigService, Server, disableDeprecatedForms(), provideForms()]);
