import { Component, EventEmitter, Output } from '@angular/core';
import { OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';
import { CardService, Card } from '../services/card.service';
import { LoginService } from '../services/login.service';

/**
 * Login Dialog
 */
@Component({
    selector: 'login',
    template: require('../../templates/loginform.view.html')
})
export class LoginFormComponent implements OnInit {
    username: string;
    password: string;
    isBusy: boolean;
    userMessage: string;

  // injected by angular DI
    constructor(private router: Router, private routeParams: RouteParams, private loginService: LoginService) { }

    ngOnInit() {

    }

// called when pressed on Login buttn
    doLogin() {
        let that = this;
        this.isBusy = true;
        // login request to the server
        this.loginService.login(this.username, this.password)
            .subscribe(
            function(message: string) {
               that.userMessage = null;
                that.router.navigate(['CardFileList']);
                that.isBusy = false;
            },
            (message: string) => {
              // if login failed, display error message
              that.userMessage = message;
              that.isBusy = false;
            }
            );
    }
}
