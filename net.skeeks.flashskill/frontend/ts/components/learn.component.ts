import {
    Component,
    Input,
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';
import { OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';
import { Card } from '../services/card.service';
import { LearnService } from '../services/learn.service';

/**
 * The 'Lernen' Site.
 */
@Component({
    selector: 'learn',
    template: require('../../templates/learn.view.html'),
    providers: [LearnService],
    animations: [ // animation for flipping the card
        trigger('learnState', [
            state('term', style({
                transform: 'rotateX(0deg)'
            })),
            state('definition', style({
                transform: 'rotateX(-180deg)'
            })),
            transition('term => definition', animate('500ms ease-out')),
            transition('definition => term', animate('500ms ease-in'))
        ])
    ]
})
export class LearnComponent implements OnInit {
    cardfileid: string;
    guess: string;
    previousGuess: string;
    currentCard: Card;
    nextCard: Card;
    isFlipped = false;
    isCorrect: boolean;
    userMessage: string;
    cardFileFinished: boolean;
    isChecking: boolean;
 // injected by angular DI
    constructor(private learnService: LearnService, private router: Router, private routeParams: RouteParams) {
    }

    // tries to load the current card
    ngOnInit() {
        let id = this.routeParams.get('id');
        var that = this;
        this.cardfileid = id;
        this.learnService.getCurrentCard(id).subscribe(
            function(card: Card) {
                that.currentCard = card;
            },
            function(message: string) {
                that.userMessage = message;
            }
        );
    }

// flips the card
    toggleState() {
        if (this.isFlipped) {
            this.isFlipped = false;
        } else {
            this.isFlipped = true;
        }
    }
 // called when a guess is submitted
    onGuess() {
        let that = this;
        if (this.nextCard) {
            this.toggleState();
            this.currentCard = this.nextCard;
            this.isCorrect = false;
            this.nextCard = null;
        } else {
            that.isChecking = true;
            // server is askes if the user was right.
            this.learnService.processGuess(that.cardfileid, that.guess).subscribe(
                (response: LearnResponseModel) => {
                    if (response.cardFileFinished) {
                        that.cardFileFinished = true;
                    } else {
                        that.previousGuess = that.guess;
                        that.guess = null;
                        that.cardFileFinished = false;
                        that.isCorrect = response.correct;
                        that.currentCard.definition = response.solution
                        that.toggleState();
                        if (that.isCorrect) {
                            that.currentCard.knownCount++;
                            that.currentCard.section++;
                        } else {
                            that.currentCard.missedCount++;
                        }
                        // next card was already delivered, set it as nextCard
                        that.nextCard = response.nextCard;
                    }
                    that.isChecking = false;
                },
                (message: string) => {
                    that.userMessage = message;
                    that.isChecking = false;
                }
            );
        }
    }
}

interface LearnResponseModel {
    correct: boolean;
    solution: string;
    nextCard: Card;
    cardFileFinished: boolean;
}
