import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { UserService, User } from '../services/user.service';
import { LoginService } from '../services/login.service';
import { RouteParams, Router } from '@angular/router-deprecated';

/**
 *  The 'Registrieren' site
 */
@Component({
    selector: 'registerform',
    template: require('../../templates/registerform.view.html')
})
export class RegisterFormComponent implements OnInit {
    username: string;
    password: string;
    isSaving: boolean = false;
    userMessage: string;
    // injected by angular DI
    constructor(private router: Router, private routeParams: RouteParams, private userService: UserService, private loginService: LoginService) { }

    ngOnInit() {
    }

    // called when 'Registrieren' button is pressed
    doRegister() {
        let that = this;
        that.isSaving = true;
        // register the user
        this.userService.registerUser(that.username, that.password).subscribe(
            (message: string) => {
                that.loginService.changeLoginState('USER');
                that.router.navigate(['CardFileList']);
                that.isSaving = false;
                that.userMessage = null;
            },
            (message: string) => {
                that.isSaving = false;
                that.userMessage = message;
            }
        );
    }
}
