import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { UserService, User } from '../services/user.service';
import { RouteParams, Router } from '@angular/router-deprecated';

@Component({
    selector: 'userform',
    template: require('../../templates/userform.view.html')
})
export class UserFormComponent implements OnInit {
    user: User;
    isSaving: boolean = false;
    isPasswordRequired: boolean;
    userMessage: string;

    //the parameter are injected by Angular DI
    constructor(private router: Router, private routeParams: RouteParams, private userService: UserService) { }

    ngOnInit() {
        let id = this.routeParams.get('id');
        let that = this;
        if (id) {
            that.userService.getUser(id).subscribe(
                (user: User) => {
                    that.user = user;
                },
                (message: string) => {
                    that.userMessage = message;
                }
            );
            that.isPasswordRequired = false;
        } else {
            that.user = new User();
            that.isPasswordRequired = true;
        }
    }

    onStore(user: User) {
        let that = this;
        that.isSaving = true;
        this.userService.storeUser(user).subscribe(
            (message: string) => {
                that.router.navigate(['UserList']);
                that.isSaving = false;
                that.userMessage = null;
            },
            (message: string) => {
                that.isSaving = false;
                that.userMessage = message;
            }
        );
    }

    onCancel() {
        this.router.navigate(['UserList']);
        this.isSaving = false;
        this.userMessage = null;
    }
}
