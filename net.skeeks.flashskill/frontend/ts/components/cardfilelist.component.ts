import { Component } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { CardFileService, CardFile } from '../services/cardfile.service.ts';

@Component({
    selector: 'cardfilelist',
    template: require('../../templates/cardfilelist.view.html')
})
export class CardFileListComponent {
    cardFiles: CardFile[];
    userMessage: string;
    isDeleting: string;
    isResetting: string;

//the parameter are injected by Angular DI
    constructor(private router: Router, private cardFileService: CardFileService) { }

    ngOnInit() {
        this.load();
    }

    onEditCardFile(cardFile: CardFile) {
        this.router.navigate(['ModifyCardFileForm', { id: cardFile.id }]);
    }

    onCreateCardFile() {
        this.router.navigate(['CreateCardFileForm']);
    }

    onStudyCardFile(cardFile: CardFile) {
        this.router.navigate(['Learn', { id: cardFile.id }]);
    }

    onDeleteCardFile(cardFile: CardFile) {
        let that = this;
        that.isDeleting = cardFile.id;
        this.cardFileService.deleteCardFile(cardFile).subscribe(
            (message: string) => {
                that.load();
                that.isDeleting = null;
            },
            (message: string) => {
                that.userMessage = message;
                that.isDeleting = null;
            }
        );
    }

    onResetCardFile(cardFile: CardFile) {
        let that = this;
        that.isResetting = cardFile.id;
        this.cardFileService.resetCardFile(cardFile).subscribe(
            (message: string) => {
                that.load();
                that.isResetting = null;
            },
            (message: string) => {
                that.userMessage = message;
                that.isResetting = null;
            }
        );
    }

    load() {
        let that = this;
        this.cardFileService.getCardFiles().subscribe(
            (cardFiles: CardFile[]) => that.cardFiles = cardFiles,
            (message: string) => {
                that.userMessage = message;
            }
        );
    }

    doOpenStatisticsPage(cardFile: CardFile){
      this.router.navigate(['StatisticsPage', {id: cardFile.id}]);
    }
}
