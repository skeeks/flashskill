import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { UserService, User } from '../services/user.service';
import { Router } from '@angular/router-deprecated';

@Component({
    selector: 'userlist',
    template: require('../../templates/userlist.view.html')
})
export class UserListComponent implements OnInit {
    users: User[];
    userMessage: string;
    isDeleting: string;

    //the parameter are injected by Angular DI
    constructor(private router: Router, private userService: UserService) { }

    ngOnInit() {
        this.load();
    }

    onEditUser(user: User) {
        this.router.navigate(['ModifyUserForm', { id: user.id }]);
    }

    onCreateUser() {
        this.router.navigate(['CreateUserForm']);
    }

    onDeleteUser(user: User) {
        let that = this;
        that.isDeleting = user.id;
        this.userService.deleteUser(user).subscribe(
            (message: string) => {
                that.load();
                that.userMessage = null;
                that.isDeleting = null;
            },
            (message: string) => {
                console.log(message);
                that.userMessage = message;
                that.isDeleting = null;
            }
        );
    }

    load() {
        let that = this;
        this.userService.getUsers().subscribe(
            (users: User[]) => {
                that.users = users
                that.userMessage = null;
            },
            (message: string) => {
                that.userMessage = message;
            }
        );
    }
}
