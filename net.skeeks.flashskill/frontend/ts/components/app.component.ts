import { Component, OnInit }       from '@angular/core';
import { RouteConfig, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass} from '@angular/common';

import { UserService } from '../services/user.service';
import { CardFileService } from '../services/cardfile.service';
import { CardService } from '../services/card.service';

import { LearnComponent } from './learn.component';
import { CardFileListComponent } from './cardfilelist.component';
import { UserListComponent } from './userlist.component';
import { ChangePasswordFormComponent } from './changepasswordform.component';
import { RegisterFormComponent } from './registerform.component';

import { CardFileFormComponent } from './cardfileform.component';
import { UserFormComponent } from './userform.component';
import { LoginFormComponent } from './loginform.component';
import { LoginService, LoginState } from '../services/login.service';

import { ErrorPageComponent } from './errorpage.component';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import {StatisticsPageComponent} from './statisticspage.component';

/**
 * Top Level Angular Component for the application. Contains the built-in
 * angular router
 * 
 */
@Component({
    selector: 'application',
    template: require('../../templates/app.view.html'),
    directives: [
        ROUTER_DIRECTIVES,
        UserListComponent,
        CardFileListComponent,
        LearnComponent,
        UserFormComponent,
        CardFileFormComponent,
        LoginFormComponent,
        StatisticsPageComponent
    ],
    providers: [
        UserService,
        CardFileService,
        CardService,
        LoginService
    ]
})
@RouteConfig([
    {
        path: '/',
        name: 'Home',
        component: CardFileListComponent
    },
    {
        path: '/users',
        name: 'UserList',
        component: UserListComponent
    },
    {
        path: '/learn',
        name: 'Learn',
        component: LearnComponent
    },
    {
        path: '/cardfiles',
        name: 'CardFileList',
        component: CardFileListComponent
    },
    {
        path: '/user/:id',
        name: 'ModifyUserForm',
        component: UserFormComponent
    },
    {
        path: '/user/create',
        name: 'CreateUserForm',
        component: UserFormComponent
    },
    {
        path: '/cardfile/:id',
        name: 'ModifyCardFileForm',
        component: CardFileFormComponent
    },
    {
        path: '/statistics/:id',
        name: 'StatisticsPage',
        component: StatisticsPageComponent
    },
    {
        path: '/cardfile/create',
        name: 'CreateCardFileForm',
        component: CardFileFormComponent
    },
    {
        path: '/login',
        name: 'LoginForm',
        component: LoginFormComponent
    },
    {
        path: '/error',
        name: 'ErrorPage',
        component: ErrorPageComponent
    },
    {
        path: '/register',
        name: 'RegisterForm',
        component: RegisterFormComponent
    },
    {
        path: '/changepassword',
        name: 'ChangePasswordForm',
        component: ChangePasswordFormComponent
    }
])
export class AppComponent implements OnInit {
    title = 'Flashskill';
    loginState: string;

 // the parameter are injected by Angular DI
    constructor(private router: Router, private loginService: LoginService) {
    }

    ngOnInit() {
        let that = this;
        let callback = function(state: LoginState) {
            switch (state) {
                case LoginState.UNAUTHORIZED:
                    that.router.navigate(['LoginForm']);
            }
        };
        // check login state on Init
        this.loginService.checkLoginState().subscribe(callback, callback);

        this.loginService.loginState$.subscribe(
            state => {
                that.loginState = LoginState[state];
            }
        )
    }

    // do the logout
    doLogout() {
        let that = this;
        this.loginService.logout().subscribe((state: LoginState) => {
            that.router.navigate(['LoginForm']);
        });
    }
}
