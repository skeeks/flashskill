import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { UserService, User } from '../services/user.service';
import { RouteParams, Router } from '@angular/router-deprecated';

/**
 * The 'Passwort ändern' Form
 */
@Component({
    selector: 'changepasswordform',
    template: require('../../templates/changepasswordform.view.html')
})
export class ChangePasswordFormComponent implements OnInit {
    newPassword: string;
    oldPassword: string;
    isSaving: boolean = false;
    userMessage: string;
 // injected by angular DI
    constructor(private router: Router, private routeParams: RouteParams, private userService: UserService) { }

    ngOnInit() {
    }

 // pushes the request to the server
    onStore() {
        let that = this;
        that.isSaving = true;
        this.userService.changeOwnPassword(that.newPassword, that.oldPassword).subscribe(
            (message: string) => {
                that.router.navigate(['CardFileList']);
                that.isSaving = false;
                that.userMessage = null;
            },
            (message: string) => {
                that.isSaving = false;
                that.userMessage = message;
            }
        );
    }
}
