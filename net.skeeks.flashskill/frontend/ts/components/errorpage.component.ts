import { Component, EventEmitter, Output } from '@angular/core';
import { OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';
/**
 * Error page (currently used for display 'Der Server ist nicht erreichbar...')
 */
@Component({
    selector: 'errorpage',
    template: require('../../templates/errorpage.view.html')
})
export class ErrorPageComponent implements OnInit {
    message: string;

    constructor(private router: Router, private routeParams: RouteParams) { }

    ngOnInit() {
        this.message = this.routeParams.get("message");
    }
}
