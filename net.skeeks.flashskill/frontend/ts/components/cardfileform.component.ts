import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';
import { CardService, Card } from '../services/card.service';
import { CardFileService, CardFile } from '../services/cardfile.service';

/**
 * Component for displaying the CardFileForm
 */
@Component({
    selector: 'cardfileform',
    template: require('../../templates/cardfileform.view.html')
})
export class CardFileFormComponent implements OnInit {
    cardFile: CardFile;
    isSaving: boolean = false;
    userMessage: string;

    // the parameter are injected by Angular DI
    constructor(private router: Router, private routeParams: RouteParams, private cardFileService: CardFileService) { }

    /**
     * if a the param 'id' isset, try to load the CardFile from the server.
     */
    ngOnInit() {
        let that = this;
        if (this.routeParams.get('id')) {
            let id = this.routeParams.get('id');
            this.cardFileService.getCardFile(id)
                .subscribe(
                (cardFile: CardFile) => {
                  that.cardFile = cardFile
                },
                (message: string) => {
                  that.userMessage = message;
                });
        } else {
            that.cardFile = new CardFile();
              that.userMessage = null;
        }
    }

    // when clicked on 'Save'
    onStore(cardFile: CardFile) {
        let that = this;
        that.isSaving = true;
        this.cardFileService.storeCardFile(cardFile)
            .subscribe(
            (message: string) => {
                that.router.navigate(['CardFileList']);
                that.isSaving = false;
                that.userMessage = null;
            },
            (message: string) => {
                that.isSaving = false;
                that.userMessage = message;
            });
    }

    // when click on 'add card'
    onAddCard() {
        if (!this.cardFile.cards) {
            this.cardFile.cards = [];
        }
        this.cardFile.cards.push(new Card());
    }

    // invoked when removing a card
    onRemoveCard(card: Card) {
        var index = this.cardFile.cards.indexOf(card);
        this.cardFile.cards.splice(index, 1);
    }

    // invoked when pressing 'cancel'
    onCancel() {
        this.router.navigate(['CardFileList']);
        this.isSaving = false;
        this.userMessage = null;
    }
}
