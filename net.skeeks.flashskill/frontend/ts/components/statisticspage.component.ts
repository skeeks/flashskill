import { Component, OnInit } from '@angular/core';
import { RouteParams, Router } from '@angular/router-deprecated';

import { CardFileService, StatisticsModel } from '../services/cardfile.service';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass} from '@angular/common';

import {CHART_DIRECTIVES} from './base-chart.component';

/**
 * The 'Statistik' site.
 * display
 */
@Component({
    selector: 'statistics',
    template: require('../../templates/statistics.view.html'),
    directives: [CHART_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass]
})
export class StatisticsPageComponent implements OnInit {
    statistics: StatisticsModel;
    totalCards: number = 0;
    isLoading: boolean = false;
    userMessage: string;
    chartType = 'pie';
    options = { // some options for the Diagram
        responsive: true,
        maintainAspectRatio: true,
        fullWidth: true,
        cutoutPercentage: 25,
        title: {
            display: true,
            text: 'Aufteilung in Abteilungen',
            fontSize: 30
        },
        legend: {
            position: 'left',
            labels: {
                boxWidth: 50,
                fontSize: 20,
                padding: 10
            }
        }

    }
    // injected by angular DI
    constructor(private router: Router, private routeParams: RouteParams, private cardFileService: CardFileService) { }

    ngOnInit() {
        this.doLoad();
    }

    doLoad() {
        let that = this;
        that.isLoading = true;
        // fetch data from server
        this.cardFileService.loadStatistics(this.routeParams.get('id')).subscribe(
            (statistics: StatisticsModel) => {
                that.statistics = statistics;
                for (var n in that.statistics.sections.values) {
                    that.totalCards += that.statistics.sections.values[n];
                }
                that.isLoading = false;
                that.userMessage = null;
            },
            (message: string) => {
                // on error, display error message to server
                that.isLoading = false;
                that.userMessage = message;
            }
        );
    }
}
