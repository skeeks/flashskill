package net.skeeks.flashskill;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import net.skeeks.flashskill.server.controllers.AuthentificationController;
import net.skeeks.flashskill.server.controllers.CardFileController;
import net.skeeks.flashskill.server.controllers.LearnRoundController;
import net.skeeks.flashskill.server.controllers.UserController;
import net.skeeks.flashskill.server.entities.dao.ICardFileDAO;
import net.skeeks.flashskill.server.entities.dao.IUserDAO;
import net.skeeks.flashskill.server.entities.dao.MorphiaCardFileDAO;
import net.skeeks.flashskill.server.entities.dao.MorphiaUserDAO;
import net.skeeks.flashskill.server.icontrollers.IAuthentificationController;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.server.icontrollers.ILearnRoundController;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.server.mongodb.DatabaseServiceProvider;
import net.skeeks.flashskill.server.mongodb.IDatabaseService;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.json.JsonService;

/**
 * Google Guice Module. Is able to bind concrete classes to interfaces. This increses the flexibility of the application. For example, we can replace IUserDao and ICardFileDao if we want to replace
 * MongoDb by another Database
 *
 */
public class ServiceModule extends AbstractModule {
	private static Injector INJECTOR_INSTANCE;

	protected static Injector getInjectorInstance() {
		if (INJECTOR_INSTANCE == null) {
			INJECTOR_INSTANCE = Guice.createInjector(new ServiceModule());
		}
		return INJECTOR_INSTANCE;
	}

	@Override
	protected void configure() {
		bind(IJsonService.class).to(JsonService.class);
		bind(IDatabaseService.class).toProvider(new DatabaseServiceProvider());
		bind(IAuthentificationController.class).to(AuthentificationController.class);
		bind(IUserController.class).to(UserController.class);
		bind(ICardFileController.class).to(CardFileController.class);
		bind(ILearnRoundController.class).to(LearnRoundController.class);
		bind(IUserDAO.class).to(MorphiaUserDAO.class);
		bind(ICardFileDAO.class).to(MorphiaCardFileDAO.class);
	}

	public static <T extends Object> T getInstance(Class<T> clazz) {
		return getInjectorInstance().getInstance(clazz);
	}
}
