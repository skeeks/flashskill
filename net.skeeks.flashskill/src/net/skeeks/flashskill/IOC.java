package net.skeeks.flashskill;

/**
 * Inverson of Control quick access.
 *
 */
public abstract class IOC {
	public static <T> T get(Class<T> clazz) {
		return ServiceModule.getInstance(clazz);
	}
}
