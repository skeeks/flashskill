package net.skeeks.flashskill.shared.models;

import java.util.Date;

/**
 * Model for a User
 *
 */
public class UserModel extends BasicModel {
	private String m_id;
	private String m_username;
	private boolean m_isAdmin;
	private String m_password;
	private Date m_lastLoginDate;

	public UserModel() {
		super(true);
	}

	public String getUsername() {
		return m_username;
	}

	public void setUsername(String username) {
		m_username = username;
	}

	public boolean isAdmin() {
		return m_isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		m_isAdmin = isAdmin;
	}

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	public String getPassword() {
		return m_password;
	}

	public void setPassword(String password) {
		m_password = password;
	}

	public Date getLastLoginDate() {
		return m_lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		m_lastLoginDate = lastLoginDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		result = prime * result + (m_isAdmin ? 1231 : 1237);
		result = prime * result + ((m_lastLoginDate == null) ? 0 : m_lastLoginDate.hashCode());
		result = prime * result + ((m_password == null) ? 0 : m_password.hashCode());
		result = prime * result + ((m_username == null) ? 0 : m_username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserModel other = (UserModel) obj;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		if (m_isAdmin != other.m_isAdmin)
			return false;
		if (m_lastLoginDate == null) {
			if (other.m_lastLoginDate != null)
				return false;
		} else if (!m_lastLoginDate.equals(other.m_lastLoginDate))
			return false;
		if (m_password == null) {
			if (other.m_password != null)
				return false;
		} else if (!m_password.equals(other.m_password))
			return false;
		if (m_username == null) {
			if (other.m_username != null)
				return false;
		} else if (!m_username.equals(other.m_username))
			return false;
		return true;
	}

}
