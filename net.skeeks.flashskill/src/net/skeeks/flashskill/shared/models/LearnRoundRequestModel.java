package net.skeeks.flashskill.shared.models;

public class LearnRoundRequestModel extends BasicModel {
	private String m_id;
	private String m_guess;

	public String getId() {
		return m_id;
	}

	public String getGuess() {
		return m_guess;
	}

	public void setGuess(String guess) {
		m_guess = guess;
	}

	public void setId(String id) {
		m_id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_guess == null) ? 0 : m_guess.hashCode());
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LearnRoundRequestModel other = (LearnRoundRequestModel) obj;
		if (m_guess == null) {
			if (other.m_guess != null)
				return false;
		} else if (!m_guess.equals(other.m_guess))
			return false;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		return true;
	}

}
