package net.skeeks.flashskill.shared.models;

public class CardModel extends BasicModel {
	private String m_id;
	private String m_term;
	private String m_definition;
	private Long m_missedCount;
	private Long m_knownCount;
	private int m_section;

	public CardModel() {
		super(true);
	}

	public String getTerm() {
		return m_term;
	}

	public void setTerm(String term) {
		m_term = term;
	}

	public String getDefinition() {
		return m_definition;
	}

	public void setDefinition(String definition) {
		m_definition = definition;
	}

	public Long getMissedCount() {
		return m_missedCount;
	}

	public void setMissedCount(Long missedCount) {
		m_missedCount = missedCount;
	}

	public int getSection() {
		return m_section;
	}

	public void setSection(int section) {
		m_section = section;
	}

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_definition == null) ? 0 : m_definition.hashCode());
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		result = prime * result + ((m_knownCount == null) ? 0 : m_knownCount.hashCode());
		result = prime * result + ((m_missedCount == null) ? 0 : m_missedCount.hashCode());
		result = prime * result + m_section;
		result = prime * result + ((m_term == null) ? 0 : m_term.hashCode());
		return result;
	}

	@Override
	public boolean getSuccessful() {
		return super.getSuccessful();
	}

	public Long getKnownCount() {
		return m_knownCount;
	}

	public void setKnownCount(Long knownCount) {
		m_knownCount = knownCount;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardModel other = (CardModel) obj;
		if (m_definition == null) {
			if (other.m_definition != null)
				return false;
		} else if (!m_definition.equals(other.m_definition))
			return false;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		if (m_knownCount == null) {
			if (other.m_knownCount != null)
				return false;
		} else if (!m_knownCount.equals(other.m_knownCount))
			return false;
		if (m_missedCount == null) {
			if (other.m_missedCount != null)
				return false;
		} else if (!m_missedCount.equals(other.m_missedCount))
			return false;
		if (m_section != other.m_section)
			return false;
		if (m_term == null) {
			if (other.m_term != null)
				return false;
		} else if (!m_term.equals(other.m_term))
			return false;
		return true;
	}

}
