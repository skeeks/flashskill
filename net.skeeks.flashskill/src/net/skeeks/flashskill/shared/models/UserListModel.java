package net.skeeks.flashskill.shared.models;

import java.util.List;

/**
 * A list of users
 *
 */
public class UserListModel extends BasicModel {
	public UserListModel() {
		super(true);
	}

	private List<UserModel> m_users;

	public List<UserModel> getUsers() {
		return m_users;
	}

	public void setUsers(List<UserModel> users) {
		m_users = users;
	}
}
