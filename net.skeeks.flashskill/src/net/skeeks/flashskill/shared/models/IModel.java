package net.skeeks.flashskill.shared.models;

import javax.servlet.http.HttpServletResponse;

/**
 * classes implementing this interface are sent to the client (or received from the client). Abstraction between database entities and frontend
 *
 */
public interface IModel {
	// allows the model modify the response shortly before it is sent back to the client
	public default HttpServletResponse execModifyResponse(HttpServletResponse response) {
		return response;
	}
}
