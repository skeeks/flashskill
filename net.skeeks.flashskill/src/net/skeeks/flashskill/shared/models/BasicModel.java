package net.skeeks.flashskill.shared.models;

/**
 * This is the BasicModel class. 
 * Contains two attribute 'successful' and 'message'.
 *
 */
public class BasicModel implements IModel {
	public static final BasicModel SUCCESS = new BasicModel(true);
	public static final BasicModel ERROR = new BasicModel(false);

	private boolean successful;
	private String message;

	public BasicModel() {
		this(true);
	}

	public BasicModel(boolean successful) {
		this.successful = successful;
	}

	public BasicModel(boolean successful, String message) {
		this.successful = successful;
		this.message = message;
	}

	public boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
