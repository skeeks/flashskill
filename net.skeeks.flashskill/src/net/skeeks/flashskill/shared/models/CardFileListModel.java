package net.skeeks.flashskill.shared.models;

import java.util.List;

public class CardFileListModel extends BasicModel {
	public CardFileListModel() {
		super(true);
	}

	private List<CardFileModel> m_cardFiles;

	public List<CardFileModel> getCardFiles() {
		return m_cardFiles;
	}

	public void setCardFiles(List<CardFileModel> cardFiles) {
		m_cardFiles = cardFiles;
	}
}
