package net.skeeks.flashskill.shared.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Server response for statistics page
 *
 */
public class CardFileStatisticsModel extends BasicModel {
	private String m_cardfilename;
	private SectionStatistics m_sections;
	private List<CardModel> m_topMissedCards;
	private List<CardModel> m_topKnownCards;

	public List<CardModel> getTopKnownCards() {
		if (m_topKnownCards == null) {
			m_topKnownCards = new ArrayList<>();
		}
		return m_topKnownCards;
	}

	public List<CardModel> getTopMissedCards() {
		if (m_topMissedCards == null) {
			m_topMissedCards = new ArrayList<>();
		}
		return m_topMissedCards;
	}

	public SectionStatistics getSections() {
		if (m_sections == null) {
			m_sections = new SectionStatistics();
		}
		return m_sections;
	}

	public void setSections(SectionStatistics sections) {
		m_sections = sections;
	}

	public void setTopKnownCards(List<CardModel> topKnownCards) {
		m_topKnownCards = topKnownCards;
	}

	public void setTopMissedCards(List<CardModel> topMissedCards) {
		m_topMissedCards = topMissedCards;
	}

	public String getCardfilename() {
		return m_cardfilename;
	}

	public void setCardfilename(String cardfilename) {
		m_cardfilename = cardfilename;
	}

	public static class SectionStatistics {
		private List<String> m_labels;
		private List<Integer> m_values;

		public List<String> getLabels() {
			if (m_values == null) {
				this.m_labels = new ArrayList<>();
			}
			return m_labels;
		}

		public List<Integer> getValues() {
			if (m_values == null) {
				this.m_values = new ArrayList<>();
			}
			return m_values;
		}

	}
}
