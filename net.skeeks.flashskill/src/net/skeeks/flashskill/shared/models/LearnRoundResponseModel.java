package net.skeeks.flashskill.shared.models;

public class LearnRoundResponseModel extends BasicModel {
	private String m_id;
	private boolean m_correct;
	private String m_solution;
	private CardModel m_nextCard;
	private boolean m_cardFileFinished;

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	public boolean isCorrect() {
		return m_correct;
	}

	public void setCorrect(boolean correct) {
		m_correct = correct;
	}

	public String getSolution() {
		return m_solution;
	}

	public void setSolution(String solution) {
		m_solution = solution;
	}

	public CardModel getNextCard() {
		return m_nextCard;
	}

	public void setNextCard(CardModel nextCard) {
		m_nextCard = nextCard;
	}

	public boolean isCardFileFinished() {
		return m_cardFileFinished;
	}

	public void setCardFileFinished(boolean cardFileFinished) {
		m_cardFileFinished = cardFileFinished;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (m_cardFileFinished ? 1231 : 1237);
		result = prime * result + (m_correct ? 1231 : 1237);
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		result = prime * result + ((m_nextCard == null) ? 0 : m_nextCard.hashCode());
		result = prime * result + ((m_solution == null) ? 0 : m_solution.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LearnRoundResponseModel other = (LearnRoundResponseModel) obj;
		if (m_cardFileFinished != other.m_cardFileFinished)
			return false;
		if (m_correct != other.m_correct)
			return false;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		if (m_nextCard == null) {
			if (other.m_nextCard != null)
				return false;
		} else if (!m_nextCard.equals(other.m_nextCard))
			return false;
		if (m_solution == null) {
			if (other.m_solution != null)
				return false;
		} else if (!m_solution.equals(other.m_solution))
			return false;
		return true;
	}

}
