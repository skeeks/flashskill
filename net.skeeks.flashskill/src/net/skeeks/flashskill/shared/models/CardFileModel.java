package net.skeeks.flashskill.shared.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model for CardFile. is sent to the client
 *
 */
public class CardFileModel extends BasicModel {

	private String m_id;
	private String m_name;
	private Date m_lastStudyDate;
	private List<CardModel> m_cards;

	public CardFileModel() {
		super(true);
	}

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}

	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	}

	public Date getLastStudyDate() {
		return m_lastStudyDate;
	}

	public void setLastStudyDate(Date lastStudied) {
		m_lastStudyDate = lastStudied;
	}

	public List<CardModel> getCards() {
		if (m_cards == null) {
			m_cards = new ArrayList<>();
		}
		return m_cards;
	}

	public void setCards(List<CardModel> cards) {
		m_cards = cards;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_cards == null) ? 0 : m_cards.hashCode());
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		result = prime * result + ((m_lastStudyDate == null) ? 0 : m_lastStudyDate.hashCode());
		result = prime * result + ((m_name == null) ? 0 : m_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardFileModel other = (CardFileModel) obj;
		if (m_cards == null) {
			if (other.m_cards != null)
				return false;
		} else if (!m_cards.equals(other.m_cards))
			return false;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		if (m_lastStudyDate == null) {
			if (other.m_lastStudyDate != null)
				return false;
		} else if (!m_lastStudyDate.equals(other.m_lastStudyDate))
			return false;
		if (m_name == null) {
			if (other.m_name != null)
				return false;
		} else if (!m_name.equals(other.m_name))
			return false;
		return true;
	}

}
