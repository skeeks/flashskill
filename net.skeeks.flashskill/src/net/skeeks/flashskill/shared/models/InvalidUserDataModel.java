package net.skeeks.flashskill.shared.models;

import javax.servlet.http.HttpServletResponse;

public class InvalidUserDataModel extends BasicModel {

	public InvalidUserDataModel(String message) {
		super(false, message);
	}

	@Override
	public HttpServletResponse execModifyResponse(HttpServletResponse response) {
		HttpServletResponse resp = super.execModifyResponse(response);
		resp.setStatus(400);
		return resp;
	}
}
