package net.skeeks.flashskill.shared.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.inject.Singleton;

/**
 * Implementation of IJsonServiec. Uses Jackson to parse and write JSOn
 * 
 * @author ske
 *
 */
@Singleton
public class JsonService implements IJsonService {
	private ObjectMapper m_mapper;

	public JsonService() {
		this.execInit();
	}

	public ObjectMapper getMapper() {
		return m_mapper;
	}

	/**
	 * Set some settings when creating service
	 */
	protected void execInit() {
		SimpleModule module = new SimpleModule();
		m_mapper = new ObjectMapper();
		m_mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false); // Empty beans should not fail
		module.addSerializer(ObjectId.class, new ObjectIdSerializer());
		m_mapper.registerModule(module);
		// custom dateformat
		m_mapper.setDateFormat(new SimpleDateFormat("dd.MM.yyyy HH:mm"));

	}

	public <T> T read(Class<T> type, InputStream input) {
		try {
			return getMapper().readValue(input, type);
		} catch (Exception e) {
			throw new RuntimeException("failed to read json", e);
		}
	}

	public <T> void write(T data, OutputStream output) {
		try {
			getMapper().writeValue(output, data);
		} catch (Exception e) {
			throw new RuntimeException("failed to write json", e);
		}
	}

	@Override
	public <T> String toJson(T data) {
		try {
			return getMapper().writeValueAsString(data);
		} catch (Exception e) {
			throw new RuntimeException("failed to write json", e);
		}
	}

	@Override
	public <T> T fromString(Class<T> dataType, String jsonStr) {
		try {
			return getMapper().readValue(jsonStr, dataType);
		} catch (Exception e) {
			throw new RuntimeException("failed to write json", e);
		}
	}

	/**
	 * ObjectId should be serializes with its string value
	 * 
	 * @author ske
	 *
	 */
	public class ObjectIdSerializer extends JsonSerializer<ObjectId> {

		@Override
		public void serialize(ObjectId value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
			jgen.writeString(value.toString());
		}
	}

}
