package net.skeeks.flashskill.shared.json;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Declaration of methods to parse and wrie JSON
 *
 */
public interface IJsonService {
	/**
	 * Write the 'data' to the OutputStream as JSOn
	 */
	public <T> void write(T data, OutputStream output);

	/**
	 * Read an object of type T from the specified input stream
	 */
	public <T> T read(Class<T> type, InputStream input);

	/**
	 * Converts an object into a json string.
	 */
	public <T> String toJson(T data);

	/**
	 * Converts a json string into an object
	 */
	public <T> T fromString(Class<T> type, String json);
}
