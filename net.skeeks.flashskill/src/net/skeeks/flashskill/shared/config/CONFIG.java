package net.skeeks.flashskill.shared.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Config class
 *
 */
public class CONFIG {
	private static HashMap<String, String> configMap;
	private static boolean loaded = false;

	public static boolean isLoaded() {
		return loaded;
	}

	/**
	 * Loads the config file into the map
	 */
	public static void load(String path) {
		File configFile = new File(path);
		ObjectMapper mapper = new ObjectMapper();
		try {

			// convert JSON string to Map
			configMap = mapper.readValue(new FileInputStream(configFile), new TypeReference<Map<String, String>>() {
			});
			loaded = true;

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the configured value for a key.
	 */
	public static String get(String key) {
		if (configMap == null) {
			throw new RuntimeException();
		}
		return configMap.get(key);
	}
}
