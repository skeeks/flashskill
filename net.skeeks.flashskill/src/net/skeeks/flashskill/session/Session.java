package net.skeeks.flashskill.session;

import java.io.Serializable;
import java.security.Key;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;

import com.fasterxml.jackson.annotation.JsonIgnore;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.shared.json.IJsonService;

/**
 * Represents the decoded JWT Session Token from the client
 * Contains information about the logged in user.
 * @author ske
 *
 */
public class Session implements Serializable {
	private static final long serialVersionUID = 1L;
	private static transient final Key JWE_KEY = new AesKey(ByteUtil.randomBytes(16));

	private UserEntity m_user;

	private transient boolean m_dirty;
	private transient String m_jwtRepresentation;

	public UserEntity getUser() {
		return m_user;
	}

	public void setUser(UserEntity user) {
		m_dirty = true;
		m_user = user;
	}

	@JsonIgnore
	public boolean isDirty() {
		return m_dirty || (m_user != null && m_user.isDirty());
	}

	public void reset() {
		m_dirty = false;
		if (m_user != null)
			m_user.reset();
	}

	public static Session fromJWT(String jwt) throws JoseException {
		JsonWebEncryption jwe = new JsonWebEncryption();
		jwe.setKey(JWE_KEY);
		jwe.setCompactSerialization(jwt);
		Session session = IOC.get(IJsonService.class).fromString(Session.class, jwe.getPayload());
		session.m_jwtRepresentation = jwt;
		return session;
	}

	public String asJWT() throws JoseException {
		if (m_jwtRepresentation != null && !isDirty()) {
			return m_jwtRepresentation;
		} else {
			JsonWebEncryption jwe = new JsonWebEncryption();
			jwe.setPayload(IOC.get(IJsonService.class).toJson(this));
			jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
			jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
			jwe.setKey(JWE_KEY);
			return jwe.getCompactSerialization();
		}
	}

	public boolean isLoggedInUser(UserEntity user) {
		if (user == null || user.getId() == null || getUser() == null || getUser().getId() == null) {
			return false;
		}
		return user.getId().equals(getUser().getId());
	}
}
