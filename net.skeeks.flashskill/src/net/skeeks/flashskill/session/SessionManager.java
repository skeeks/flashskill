package net.skeeks.flashskill.session;

/**
 * Used to access the current Session,
 * works with ThreadLocal mechanism
 * @author ske
 *
 */
public class SessionManager {
	// thread local stores an object per Thread.
	public static final ThreadLocal<Session> userThreadLocal = new ThreadLocal<>();

	public static void set(Session session) {
		userThreadLocal.set(session);
	}

	public static void unset() {
		userThreadLocal.remove();
	}

	public static Session get() {
		return userThreadLocal.get();
	}
	
}
