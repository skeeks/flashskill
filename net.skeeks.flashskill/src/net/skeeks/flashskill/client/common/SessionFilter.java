package net.skeeks.flashskill.client.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.jose4j.lang.JoseException;

import net.skeeks.flashskill.session.Session;
import net.skeeks.flashskill.session.SessionManager;

/**
 * This Session Filter parses the JWT from the specific header and put it into SessionManager. With this mechanism, the current session can always be retrieved.
 *
 */
@WebFilter(filterName = "SessionFilter")
public class SessionFilter implements Filter {
	public static final String JWT_HEADER_NAME = "X-JWT"; // header name of JWT token

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		Session session = null;
		if (!StringUtils.isBlank(request.getHeader(JWT_HEADER_NAME))) { // check if header with JWT token is available
			try {
				session = Session.fromJWT(request.getHeader(JWT_HEADER_NAME).trim()); // parse JWT header to Session
			} catch (JoseException e) {
				LOG.error("The session could not be decrypted. New session is going to be created");
			}
		}
		if (session == null) { // if user is new (has no session yet), create a new one
			session = new Session();
		}
		SessionManager.set(session);
		chain.doFilter(request, response); // continue with servlets etc.
		SessionManager.unset(); // after request chain, unset the Session so that it does not unnecessarily needs memory
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}
}
