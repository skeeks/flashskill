package net.skeeks.flashskill.client.common;

/**
 * Exceptions of this type can be showed to the user, so ensure that the message does not contain risky information.
 * 
 * @author ske
 *
 */
public class UserException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UserException() {
		super();
	}

	public UserException(String message) {
		super(message);
	}

	public UserException(String message, Throwable t) {
		super(message, t);
	}

	public UserException(Throwable t) {
		super(t);
	}
}
