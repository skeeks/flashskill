package net.skeeks.flashskill.client.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.session.Session;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;

/**
 * This servlet Filter protects from unauthorized access. Currently, all pages under /sapi/ (secure api) are protected by this servlet.
 */
@WebFilter(filterName = "AuthentificationFilter")
public class AuthentificationFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * Checks if the user object on the session is set. If not, the user is not logged in and we return an unauthorized error.
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		Session session = SessionManager.get();
		if (session.getUser() == null) { // if no user is set on the JWT Session, we throw a http forbidden error.
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			IOC.get(IJsonService.class).write(new BasicModel(false, "Sie sind nicht berechtigt, diese Daten zu sehen!"), response.getOutputStream());
			response.flushBuffer();
			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
	}
}
