package net.skeeks.flashskill.client.common;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.config.CONFIG;

/**
 * Ensures that the CONFIG is loaded when the application starts up
 * @author ske
 *
 */
@WebListener
public class FlashskillStartupListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent context) {
		if (!CONFIG.isLoaded()) {
			CONFIG.load(context.getServletContext().getRealPath("/WEB-INF/config.json"));
		}
		IOC.get(IUserController.class).ensureAdminUserExists();
	}

}
