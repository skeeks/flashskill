package net.skeeks.flashskill.client.common;

/**
 * static LOG class which centralizes the logging. Currently uses only System.err as output
 * 
 * @author ske
 *
 */
public class LOG {

	public static void info(String... messages) {
		System.err.print("INFO: ");
		for (String msg : messages) {
			System.err.print(msg);
		}
		System.err.println();
	}

	public static void warn(String... messages) {
		System.err.print("WARN: ");
		for (String msg : messages) {
			System.err.print(msg);
		}
		System.err.println();
	}

	public static void error(String msg, Exception e) {
		System.err.print("ERROR: ");
		System.err.println(msg);
		if (e != null)
			e.printStackTrace(System.err);
		System.err.println();
	}

	public static void error(String... messages) {
		System.err.print("ERROR: ");
		for (String msg : messages) {
			System.err.print(msg);
		}
		System.err.println();
	}
}
