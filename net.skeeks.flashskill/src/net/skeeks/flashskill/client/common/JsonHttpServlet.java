package net.skeeks.flashskill.client.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jose4j.lang.JoseException;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Abstract servlet which does some common work for all the implementing servlet, like ErrorHandling, ensuring JSON is returned.
 *
 */
public class JsonHttpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Set the response to JSON. also append X-JWT cookie
	 */
	protected void doJsonRequest(HttpServletRequest req, HttpServletResponse response, IModel jsonResponse) throws ServletException, IOException {
		response.setHeader("Content-Type", "application/json"); // header is set to JSOn
		response.setCharacterEncoding("utf-8"); // utf-8 encoding for special characters
		if (SessionManager.get() != null) { // if Session is set, the put the encoded Sesssion (JWT) as header to the response
			try {
				response.setHeader(SessionFilter.JWT_HEADER_NAME, SessionManager.get().asJWT());
			} catch (JoseException e) {
				LOG.error("Could not serialize Session into JWT.", e);
			}
		}
		if (jsonResponse == null) {
			response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		}
		response = jsonResponse.execModifyResponse(response); // allows the model modify the response shortly before it is sent back to the client
		IOC.get(IJsonService.class).write(jsonResponse, response.getOutputStream()); // write the model as JSON to the response Body
	}

	/**
	 * Wraps the method handleGet
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			this.doJsonRequest(req, resp, this.handleGet(req, resp));
		} catch (Exception e) {
			this.handleException(req, resp, e);
		}
	}

	/**
	 * This methods handles an exception.
	 * 
	 * @param req
	 * @param resp
	 * @param e
	 * @throws IOException
	 */
	private void handleException(HttpServletRequest req, HttpServletResponse resp, Exception e) throws IOException {
		String message = "Es ist ein unerwarteter Fehler aufgetreten. Bitte kontaktiere den Administrator."; // default message (do not tell about details for security concerns)
		if (e instanceof SecurityException) { // if security exception, a http forbidden error should be presented
			resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
			message = e.getMessage();
		} else if (e instanceof UserException) { // a UserException can be showed to the user
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			message = e.getMessage();
		} else {
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); // all other errors are internal server errors (not expected)
		}

		e.printStackTrace(); // stack trace is printed so that we can view the errors on the server
		IOC.get(IJsonService.class).write(new BasicModel(false, message), resp.getOutputStream()); // write the "ErrorModel" to the response body
	}

	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		return null;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			this.doJsonRequest(req, resp, this.handlePost(req, resp));
		} catch (Exception e) {
			this.handleException(req, resp, e);
		}
	}

	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		return null;
	}

}
