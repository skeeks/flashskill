package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ILearnRoundController;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.CardModel;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.LearnRoundRequestModel;
import net.skeeks.flashskill.shared.models.LearnRoundResponseModel;

/**
 * Servlets represent the 2th tier of the application
 * Endpoint for learning a cardfile
 * @author ske
 *
 */
@WebServlet("/sapi/learn")
public class LearnServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Gets the current card to learn.
	 */
	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String cardFileId = StringUtils.trim(req.getParameter("cardfileid"));
		CardModel model = IOC.get(ILearnRoundController.class).getCurrentCard(cardFileId);

		return model;
	}

	/**
	 * Checks if the entered definition/term is correct, returns the result and if the word was correct, the next card.
	 */
	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LearnRoundRequestModel learnRequestModel = IOC.get(IJsonService.class).read(LearnRoundRequestModel.class, req.getInputStream());

		learnRequestModel.setGuess(StringUtils.trim(learnRequestModel.getGuess()));
		LearnRoundResponseModel model = IOC.get(ILearnRoundController.class).processLearn(learnRequestModel);

		return model;
	}

}
