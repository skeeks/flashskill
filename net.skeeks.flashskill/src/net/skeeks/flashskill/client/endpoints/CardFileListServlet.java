package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.shared.models.CardFileListModel;
import net.skeeks.flashskill.shared.models.CardFileModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Servlets represent the 2th tier of the application
 *
 */
@WebServlet("/sapi/cardfiles")
public class CardFileListServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Delivers the list of all cardfiles of an user.
	 */
	protected IModel handleGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<CardFileModel> cardFiles = IOC.get(ICardFileController.class).getCardFiles();

		CardFileListModel model = new CardFileListModel();
		model.setCardFiles(cardFiles);
		return model;
	}

}
