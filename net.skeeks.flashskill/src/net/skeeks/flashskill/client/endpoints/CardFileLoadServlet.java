package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.shared.models.CardFileModel;
import net.skeeks.flashskill.shared.models.IModel;

@WebServlet("/sapi/cardfile/load")
public class CardFileLoadServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Gets the data for a cardfile.
	 */
	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String cardFileId = StringEscapeUtils.escapeHtml4(StringUtils.trim(req.getParameter("cardfileid")));

		CardFileModel cardFile = IOC.get(ICardFileController.class).load(cardFileId);

		return cardFile;
	}

}
