package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.CardFileModel;
import net.skeeks.flashskill.shared.models.CardModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet("/sapi/cardfile/store")
public class CardFileStoreServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Updates a cardfile.
	 */
	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CardFileModel cardFile = IOC.get(IJsonService.class).read(CardFileModel.class, req.getInputStream());

		// html escaping is done by angular
		cardFile.setName(StringUtils.trim(cardFile.getName()));
		for (CardModel card : cardFile.getCards()) {
			card.setDefinition(StringUtils.trim(card.getDefinition()));
			card.setTerm(StringUtils.trim(card.getTerm()));
		}

		// store when ID is already set, else create
		if (cardFile.getId() == null) {
			IOC.get(ICardFileController.class).create(cardFile);
		} else {
			IOC.get(ICardFileController.class).save(cardFile);
		}
		return BasicModel.SUCCESS;

	}
}
