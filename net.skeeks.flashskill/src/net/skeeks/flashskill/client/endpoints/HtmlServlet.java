package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet ensures, that there is no 404 Error when we reload the application. Angular changes the url while you navigate in the application. We catch all possible urls and always deliver
 * index.html.
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet(urlPatterns = { "/learn", "/users", "/cardfiles", "/user/*", "/cardfile/*", "/login", "/logout", "/error", "/overview", "/register", "/changepassword", "/statistics/*" })
public class HtmlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Delivers the HTML Page which includes the Frontend
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.addHeader("Cache-Control", "max-age=86400, public, must-revalidate'");
		req.getServletContext().getRequestDispatcher("/index.html").forward(req, resp);
	}
}
