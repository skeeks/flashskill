package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.UserModel;

/**
 * Servlets represent the 2th tier of the application
 * 
 * @author ske
 *
 */
@WebServlet("/sapi/user/store")
public class UserStoreServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Endpoint to store an user
	 */
	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserModel userModel = IOC.get(IJsonService.class).read(UserModel.class, req.getInputStream());

		userModel.setUsername(StringUtils.trim(userModel.getUsername()));

		if (userModel.getId() == null) {
			IOC.get(IUserController.class).create(userModel);
		} else {
			IOC.get(IUserController.class).save(userModel);
		}
		return BasicModel.SUCCESS;

	}
}
