package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.IAuthentificationController;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.json.JsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.InvalidUserDataModel;
import net.skeeks.flashskill.shared.models.RegisterRequestModel;
import net.skeeks.flashskill.shared.models.UserModel;

/**
 * Servlets represent the 2th tier of the application
 * 
 * @author ske
 *
 */
@WebServlet("/api/register")
public class RegisterServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected IModel handlePost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		RegisterRequestModel model = IOC.get(JsonService.class).read(RegisterRequestModel.class, request.getInputStream());

		if (StringUtils.isEmpty(model.getUsername()) || StringUtils.isEmpty(model.getPassword())) {
			return new InvalidUserDataModel("Der Benutzername und/oder das Passwort d�rfen nicht leer sein");
		}

		String cleanedUsername = StringUtils.trim(model.getUsername());
		model.setUsername(cleanedUsername);

		String cleanedPassword = StringUtils.trim(model.getPassword());
		model.setPassword(cleanedPassword);

		UserModel user = new UserModel();
		user.setPassword(cleanedPassword);
		user.setUsername(cleanedUsername);

		user = IOC.get(IUserController.class).register(user);

		IOC.get(IAuthentificationController.class).login(model);

		return BasicModel.SUCCESS;
	}
}
