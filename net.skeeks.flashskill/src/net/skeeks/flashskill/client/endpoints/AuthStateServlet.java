package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Returns if the requester is a logged in user and if yes, if hes an admin
 * @author ske
 *
 */
@WebServlet("/api/authstate")
public class AuthStateServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (SessionManager.get().getUser() == null) {
			return new BasicModel(true, "UNAUTHORIZED");
		} else if(SessionManager.get().getUser().isAdmin()) {
			return new BasicModel(true, "ADMIN");
		} else {
			return new BasicModel(true, "USER");
		}
	}
}
