package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.shared.models.CardFileStatisticsModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Servlets represent the 2th tier of the application
 * delivers statistics to the client
 * @author ske
 *
 */
@WebServlet(urlPatterns = { "/sapi/cardfile/statistics" })
public class CardFileStatisticsServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Generate some statistics about a CardFile and deliver it to the client
	 */
	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String cardFileId = StringEscapeUtils.escapeHtml4(StringUtils.trim(req.getParameter("cardfileid")));

		CardFileStatisticsModel cardFile = IOC.get(ICardFileController.class).loadStatistics(cardFileId);

		return cardFile;
	}
}
