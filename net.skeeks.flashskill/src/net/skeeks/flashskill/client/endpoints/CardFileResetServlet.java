package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.IdModel;

/**
 * Servlets represent the 2th tier of the application endpoint to reset a cardFile
 *
 */
@WebServlet("/sapi/cardfile/reset")
public class CardFileResetServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Updates a cardfile.
	 */
	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		IdModel model = IOC.get(IJsonService.class).read(IdModel.class, req.getInputStream());

		if (model != null) {
			IOC.get(ICardFileController.class).reset(model.getId());
			return BasicModel.SUCCESS;
		} else {
			throw new RuntimeException("Keine Daten empfangen");
		}

	}
}
