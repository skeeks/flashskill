package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.json.IJsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.ChangePasswordModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet("/sapi/user/changepassword")
public class ChangePasswordServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ChangePasswordModel model = IOC.get(IJsonService.class).read(ChangePasswordModel.class, req.getInputStream());

		String cleanedNewPassword = StringUtils.trim(model.getNewPassword());
		model.setNewPassword(cleanedNewPassword);

		String cleanedOldPassword = StringUtils.trim(model.getOldPassword());
		model.setOldPassword(cleanedOldPassword);

		IOC.get(IUserController.class).changePassword(model.getNewPassword(), model.getOldPassword());

		return BasicModel.SUCCESS;
	}
}
