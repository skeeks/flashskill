package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.UserModel;


/**
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet("/sapi/user/load")
public class UserLoadServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Endpoint to load a user by its ID
	 */
	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userId = StringEscapeUtils.escapeHtml4(StringUtils.trim(req.getParameter("userid")));

		UserModel user = IOC.get(IUserController.class).load(userId);

		return user;
	}

}
