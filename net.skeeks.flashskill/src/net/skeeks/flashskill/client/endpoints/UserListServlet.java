package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.UserListModel;
import net.skeeks.flashskill.shared.models.UserModel;

/**
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet(urlPatterns = { "/sapi/users" })
public class UserListServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected IModel handleGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<UserModel> users = IOC.get(IUserController.class).getUsers();
		UserListModel userList = new UserListModel();
		userList.setUsers(users);
		return userList;
	}
}
