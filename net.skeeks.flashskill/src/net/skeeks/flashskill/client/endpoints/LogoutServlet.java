package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;

/**
 * Servlets represent the 2th tier of the application
 * 
 * @author ske
 *
 */
@WebServlet(urlPatterns = { "/sapi/logout" })
public class LogoutServlet extends JsonHttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected IModel handlePost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		SessionManager.unset();
		return BasicModel.SUCCESS;
	}
}
