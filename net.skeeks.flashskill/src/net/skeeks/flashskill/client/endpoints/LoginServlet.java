package net.skeeks.flashskill.client.endpoints;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.JsonHttpServlet;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.server.icontrollers.IAuthentificationController;
import net.skeeks.flashskill.shared.json.JsonService;
import net.skeeks.flashskill.shared.models.BasicModel;
import net.skeeks.flashskill.shared.models.IModel;
import net.skeeks.flashskill.shared.models.InvalidUserDataModel;
import net.skeeks.flashskill.shared.models.LoginRequestModel;

/**
 * Servlets represent the 2th tier of the application
 * @author ske
 *
 */
@WebServlet("/api/login")
public class LoginServlet extends JsonHttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Checks if the given credentials are valid and if yes, logs the user in.
	 */
	protected IModel handlePost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginRequestModel model = IOC.get(JsonService.class).read(LoginRequestModel.class, request.getInputStream());

		if (StringUtils.isEmpty(model.getUsername()) || StringUtils.isEmpty(model.getPassword())) {
			return new InvalidUserDataModel("Der Benutzername und/oder das Passwort d�rfen nicht leer sein");
		}

		String cleanedUsername = StringUtils.trim(model.getUsername());
		model.setUsername(cleanedUsername);

		String cleanedPassword = StringUtils.trim(model.getPassword());
		model.setPassword(cleanedPassword);

		UserEntity user = IOC.get(IAuthentificationController.class).login(model);

		if (user == null) {
			return new InvalidUserDataModel("Der Benutzername oder das Passwort stimmt nicht!");
		} else {
			if (user.isAdmin()) {
				return new BasicModel(true, "ADMIN");
			} else {
				return new BasicModel(true, "USER");
			}
		}

	}

}
