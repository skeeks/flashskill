package net.skeeks.flashskill.server.common;

/**
 * exception thrown by LearnRoundController, when the all Cards
 * are in the last section and you need to reset the CardFile to learn again.
 * @author ske
 *
 */
public class CardFileFinishedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CardFileFinishedException(String message) {
		super(message);
	}
}
