package net.skeeks.flashskill.server.icontrollers;

import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.shared.models.LoginRequestModel;

/**
 * Controller represent the 3th tier in the Application
 * This controlle is responsible for log in & out users 
 *
 */
public interface IAuthentificationController {
	public UserEntity login(LoginRequestModel username);

	void logout();

}
