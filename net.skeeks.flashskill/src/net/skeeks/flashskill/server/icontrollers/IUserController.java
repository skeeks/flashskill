package net.skeeks.flashskill.server.icontrollers;

import java.util.List;

import net.skeeks.flashskill.shared.models.UserModel;

/**
 * Controllers represent the 3th tier of the application this controller is responsible for all operations with user
 */
public interface IUserController {
	/**
	 * Create a new user
	 */
	public UserModel create(UserModel user);

	/**
	 * Store an user
	 */
	public UserModel save(UserModel user);

	/**
	 * Delete an user
	 */
	void delete(String userId);

	/**
	 * check ifs an admin user exists, and if not, creates one.
	 */
	public void ensureAdminUserExists();

	/**
	 * Load all users
	 */
	public List<UserModel> getUsers();

	/**
	 * Load a specific user
	 * 
	 * @return
	 */
	public UserModel load(String userId);

	/**
	 * Change password of logged in user.
	 */
	public void changePassword(String newPassword, String oldPassword);

	/**
	 * Register a new User
	 * 
	 */
	public UserModel register(UserModel user);
}
