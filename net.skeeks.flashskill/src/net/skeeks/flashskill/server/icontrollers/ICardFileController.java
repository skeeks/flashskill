package net.skeeks.flashskill.server.icontrollers;

import java.util.List;

import net.skeeks.flashskill.shared.models.CardFileModel;
import net.skeeks.flashskill.shared.models.CardFileStatisticsModel;
/**
 * Controller represent the 3th tier in the Application
 * this controller is responsible for all operations with CardFiles
 *
 */
public interface ICardFileController {
	/**
	 * Load a cardFile
	 * @param cardFileId
	 */
	public CardFileModel load(String cardFileId);

	/**
	 * Create a CardFile
	 * @param cardFile
	 * @return
	 */
	public CardFileModel create(CardFileModel cardFile);

	/**
	 * Delete a CardFile
	 * @param cardFileId
	 */
	public void delete(String cardFileId);

	/**
	 * Load all CardFile's from the logged in user
	 * @return
	 */
	public List<CardFileModel> getCardFiles();

	/**
	 * Reset the specified cardFile
	 * @param cardFileId
	 */
	void reset(String cardFileId);

	/**
	 * Save a already created CardFile
	 * @param responseBean
	 */
	void save(CardFileModel responseBean);

	/**
	 * Calculate some statistics data about a CardFile
	 */
	public CardFileStatisticsModel loadStatistics(String cardFileId);

}
