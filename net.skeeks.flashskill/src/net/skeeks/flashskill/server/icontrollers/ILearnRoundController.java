package net.skeeks.flashskill.server.icontrollers;

import net.skeeks.flashskill.shared.models.CardModel;
import net.skeeks.flashskill.shared.models.LearnRoundRequestModel;
import net.skeeks.flashskill.shared.models.LearnRoundResponseModel;

/**
 * Controllers represent the 3th tier of the application this controller contains operations for learning a CardFile
 * 
 * @author ske
 *
 */
public interface ILearnRoundController {

	/**
	 * Get the Current card to learn
	 */
	public CardModel getCurrentCard(String cardFileId);

	/**
	 * Check the submitted solution, returns the result and also the next card.
	 */
	public LearnRoundResponseModel processLearn(LearnRoundRequestModel learnRequestModel);
}
