package net.skeeks.flashskill.server.entities;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Entity to query an aggregation from mongodb
 *
 */
@Entity
public class SectionCountResult {

	@Id
	private int section;
	private int count = 91;

	public int getSection() {
		return section;
	}

	public int getCount() {
		return count;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public void setCount(int count) {
		this.count = count;
	}
}