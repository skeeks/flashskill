package net.skeeks.flashskill.server.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import net.skeeks.flashskill.server.common.CardFileFinishedException;

/**
 * DatabaseEntity for a CardFile.  Stored directly into the database
 * 
 * A CardFile is owned by a user and contains zero or more Cards. It has also a name and a lastStudied date.
 */
@Entity(noClassnameStored = true)
public class CardFileEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	public static final int MAX_SECTION_NUMBER = 4;

	@Id
	private ObjectId id;

	private Date lastStudied;

	private String name;

	@Reference
	private UserEntity owner;

	@Embedded
	private List<CardEntity> cards;

	@Embedded
	private LearnRoundEntity learnRound;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public List<CardEntity> getCards() {
		if (cards == null) {
			cards = new ArrayList<CardEntity>();
		}
		return cards;
	}

	public Date getLastStudied() {
		return lastStudied;
	}

	public UserEntity getOwner() {
		return owner;
	}

	public void setCards(List<CardEntity> cards) {
		this.cards = cards;
	}

	public void setLastStudied(Date lastStudied) {
		this.lastStudied = lastStudied;
	}

	public void setOwner(UserEntity owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LearnRoundEntity getLearnRound() {
		if (learnRound == null) {
			learnRound = createNewLearnRound();
		} else {
			if (learnRound.getCards().size() == 0) {
				learnRound = createNewLearnRound();
			}
		}
		return learnRound;
	}

	public void setLearnRound(LearnRoundEntity learnRound) {
		this.learnRound = learnRound;
	}

	/**
	 * Create a new learn round. Only cards, which are not in the MAX_SECTION_NUMBER section are displayed.
	 * 
	 * @throws CardFileFinishedException
	 *             when all cards of the CardFile are in the MAX_SECTION_NUMBER section.
	 */
	protected LearnRoundEntity createNewLearnRound() {
		LearnRoundEntity round = new LearnRoundEntity();
		round.setId(this.getId());

		boolean allCardsInMaxSection = true;
		for (CardEntity card : this.getCards()) {
			if (card.getSection() < MAX_SECTION_NUMBER) {
				allCardsInMaxSection = false;
				round.getCards().add(card.getId());
			}
		}
		if (allCardsInMaxSection) {
			throw new CardFileFinishedException("Alle Karten sind in Abteilung 5. Gl�ckwunsch, du hast alle gelernt!");
		}

		Collections.shuffle(round.getCards());

		return round;
	}

	/**
	 * Get a card of this cardFile by its ID
	 */
	public CardEntity getCardById(ObjectId cardFileId) {
		for (CardEntity card : getCards()) {
			if (card.getId() != null && card.getId().equals(cardFileId)) {
				return card;
			}
		}
		return null;
	}

	/**
	 * Retrieve the current learn card from the learn round.
	 * 
	 * @return
	 */
	public CardEntity getCurrentLearnCard() {
		if (getLearnRound().getCards().size() == 0)
			return null;
		ObjectId cardId = getLearnRound().getCards().get(0);
		if (cardId == null)
			return null;
		return getCardById(cardId);
	}

	public CardEntity removeCurrentLearnCard() {
		if (getLearnRound().getCards().size() == 0)
			return null;
		ObjectId cardId = getLearnRound().getCards().remove(0);
		return getCardById(cardId);
	}

}
