package net.skeeks.flashskill.server.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * DatabaseEntity for a Card. Embedded in CardFileEntity
 * 
 * Consists out of a term and definition. Has also attribute like missedCount, knownCount and the current section.
 */
@Entity(noClassnameStored = true)
public class CardEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private String term;
	private String definition;

	private long missedCount;
	private long knownCount;
	private int section;

	public String getDefinition() {
		return definition;
	}

	public String getTerm() {
		return term;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public ObjectId getId() {
		return id;
	}

	public Long getKnownCount() {
		return knownCount;
	}

	public Long getMissedCount() {
		return missedCount;
	}

	public int getSection() {
		return section;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public void setKnownCount(Long knewCount) {
		this.knownCount = knewCount;
	}

	public void setMissedCount(Long missedCount) {
		this.missedCount = missedCount;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public void nextSection() {
		this.section++;
	}

	public void resetSection() {
		this.section = 0;
	}

	public void increaseKnownCount() {
		this.knownCount++;
	}

	public void increaseMissedCount() {
		this.missedCount++;
	}
}
