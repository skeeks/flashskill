package net.skeeks.flashskill.server.entities;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Contains information about the current learn round.
 * When you start learning a CardFile, a new LearnRoundEntity is created.
 * This entity contains the order in which the Cards of the CardFile are asked.
 * If a LearnRound has no more Cards, the next LearnRound ist created.
 * 
 * Embedded in CardFileEntity
 * @author ske
 *
 */
@Entity(noClassnameStored = true)
public class LearnRoundEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private List<ObjectId> cards;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public List<ObjectId> getCards() {
		if (cards == null) {
			cards = new ArrayList<>();
		}
		return cards;
	}

	public void setCards(List<ObjectId> cards) {
		this.cards = cards;
	}
}
