package net.skeeks.flashskill.server.entities;

import java.io.Serializable;

import org.bson.types.ObjectId;

/**
 * All Beans which are stored to the database must implement this feature. Every object stored to the Database has an ID (getter & setter)
 *
 */
public interface IEntity extends Serializable {
	ObjectId getId();

	void setId(ObjectId id);

}
