package net.skeeks.flashskill.server.entities;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * DatabaseEntity for a User. Stored directly into the database
 *
 * User has a username, password and a salt. isAdmin says if the user has permission to create/read/delete/update other users.
 */
@Entity(noClassnameStored = true)
public class UserEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private String username;

	private String password;

	private String salt;

	private Date lastLoginDate;

	private boolean isAdmin;

	@Transient
	private boolean isDirty = false;

	@Reference(lazy = true)
	private List<CardFileEntity> cardFiles;

	public ObjectId getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setId(ObjectId id) {
		this.isDirty = true;
		this.id = id;
	}

	public void setPassword(String password) {
		this.isDirty = true;
		this.password = password;
	}

	public void setUsername(String username) {
		this.isDirty = true;
		this.username = username;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.isDirty = true;
		this.salt = salt;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.isDirty = true;
		this.lastLoginDate = lastLoginDate;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isDirty = true;
		this.isAdmin = isAdmin;
	}

	@JsonIgnore
	public boolean isDirty() {
		return isDirty;
	}

	public void reset() {
		isDirty = false;
	}
}
