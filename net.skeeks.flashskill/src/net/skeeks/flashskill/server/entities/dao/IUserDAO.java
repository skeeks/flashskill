package net.skeeks.flashskill.server.entities.dao;

import java.util.List;

import net.skeeks.flashskill.server.entities.UserEntity;

/**
 * DataAccessObject for UserEntity
 * Contains various methods to load/store/delete User's.
 * Abstraction between 3th and 4th Tier. 
 */
public interface IUserDAO {

	UserEntity loadById(String id);

	UserEntity loadByUsername(String username);

	void store(UserEntity user);

	void deleteById(String userId);

	List<UserEntity> loadAll();

}
