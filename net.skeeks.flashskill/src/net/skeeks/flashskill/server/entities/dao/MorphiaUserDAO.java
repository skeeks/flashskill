package net.skeeks.flashskill.server.entities.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.query.Query;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.server.mongodb.IDatabaseService;

/**
 * DAO implementation for Morphia (MongoDB)
 * @author ske
 *
 */
public class MorphiaUserDAO implements IUserDAO {
	@Override
	public UserEntity loadById(String id) {
		Query<UserEntity> query = IOC.get(IDatabaseService.class).query(UserEntity.class);
		query.field("_id").equal(new ObjectId(id));
		List<UserEntity> users = IOC.get(IDatabaseService.class).find(query);
		if (users.size() == 0) {
			return null;
		} else {
			return users.get(0);
		}
	}

	@Override
	public UserEntity loadByUsername(String username) {
		Query<UserEntity> query = IOC.get(IDatabaseService.class).query(UserEntity.class);
		query.field("username").equal(username);
		List<UserEntity> users = IOC.get(IDatabaseService.class).find(query);
		if (users.size() == 0) {
			return null;
		} else {
			return users.get(0);
		}
	}

	@Override
	public void store(UserEntity user) {
		IOC.get(IDatabaseService.class).store(user);
	}

	@Override
	public void deleteById(String userId) {
		IOC.get(IDatabaseService.class).delete(UserEntity.class, userId);
	}

	@Override
	public List<UserEntity> loadAll() {
		Query<UserEntity> query = IOC.get(IDatabaseService.class).query(UserEntity.class);
		List<UserEntity> users = IOC.get(IDatabaseService.class).find(query);
		return users;
	}
}
