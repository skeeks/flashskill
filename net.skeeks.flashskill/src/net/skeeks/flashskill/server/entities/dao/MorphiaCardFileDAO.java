package net.skeeks.flashskill.server.entities.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.aggregation.Accumulator;
import org.mongodb.morphia.aggregation.Group;
import org.mongodb.morphia.query.Query;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.entities.CardEntity;
import net.skeeks.flashskill.server.entities.CardFileEntity;
import net.skeeks.flashskill.server.entities.SectionCountResult;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.server.mongodb.IDatabaseService;

/**
 * DAO implementation for Morphia (MongoDB)
 * 
 * @author ske
 *
 */
public class MorphiaCardFileDAO implements ICardFileDAO {
	@Override
	public CardFileEntity loadById(String id) {
		Query<CardFileEntity> query = IOC.get(IDatabaseService.class).query(CardFileEntity.class);
		query.field("_id").equal(new ObjectId(id));
		List<CardFileEntity> cardFiles = IOC.get(IDatabaseService.class).find(query);
		return cardFiles.get(0);
	}

	@Override
	public void store(CardFileEntity cardFile) {
		IOC.get(IDatabaseService.class).store(cardFile);

	}

	@Override
	public void deleteById(String cardFileId) {
		IOC.get(IDatabaseService.class).delete(CardFileEntity.class, cardFileId);
	}

	@Override
	public List<CardFileEntity> loadAllByUser(UserEntity user) {
		Query<CardFileEntity> query = IOC.get(IDatabaseService.class).query(CardFileEntity.class);
		query.field("owner").equal(user);
		List<CardFileEntity> cardFiles = IOC.get(IDatabaseService.class).find(query);
		return cardFiles;
	}

	@Override
	public void deleteAllofUser(UserEntity user) {
		Query<CardFileEntity> query = IOC.get(IDatabaseService.class).query(CardFileEntity.class);
		query.field("owner").equal(user);
		IOC.get(IDatabaseService.class).delete(query);
	}

	@Override
	public List<SectionCountResult> loadGroupBySections(String cardFileId) {
		Query<CardFileEntity> query = IOC.get(IDatabaseService.class).query(CardFileEntity.class).field("_id").equal(new ObjectId(cardFileId));
		Iterator<SectionCountResult> dbIterator = IOC.get(IDatabaseService.class).getDatastore()
				.createAggregation(CardFileEntity.class)
				.match(query)
				.unwind("cards")
				.group("cards.section", Group.grouping("count", new Accumulator("$sum", 1)))
				.aggregate(SectionCountResult.class);

		List<SectionCountResult> results = new ArrayList<>();
		while (dbIterator.hasNext()) {
			results.add(dbIterator.next());
		}
		Collections.sort(results, new Comparator<SectionCountResult>() {
			@Override
			public int compare(SectionCountResult o1, SectionCountResult o2) {
				return o1.getSection() > o2.getSection() ? 1 : (o1.getSection() == o2.getSection() ? 0 : -1);
			}
		});
		return results;
	}

	@Override
	public List<CardEntity> loadTop5KnownCards(String cardFileId) {
		CardFileEntity cardFile = loadById(cardFileId);

		// sort the cards after the knownCount
		Collections.sort(cardFile.getCards(), new Comparator<CardEntity>() {
			@Override
			public int compare(CardEntity o1, CardEntity o2) {
				return o1.getKnownCount() > o2.getKnownCount() ? -1 : (o1.getKnownCount() == o2.getKnownCount() ? 0 : 1);
			}
		});
		List<CardEntity> cards = new ArrayList<>(5);
		for (CardEntity card : cardFile.getCards()) {
			cards.add(card);
			if (cards.size() == 5) {
				return cards;
			}
		}
		return cards;
	}

	@Override
	public List<CardEntity> loadTop5MissedCards(String cardFileId) {
		CardFileEntity cardFile = loadById(cardFileId);
		// sort the cards after the missedCount
		Collections.sort(cardFile.getCards(), new Comparator<CardEntity>() {
			@Override
			public int compare(CardEntity o1, CardEntity o2) {
				return o1.getMissedCount() > o2.getMissedCount() ? -1 : (o1.getMissedCount() == o2.getMissedCount() ? 0 : 1);
			}
		});
		List<CardEntity> cards = new ArrayList<>(5);
		for (CardEntity card : cardFile.getCards()) {
			cards.add(card);
			if (cards.size() == 5) {
				return cards;
			}
		}
		return cards;

	}

}
