package net.skeeks.flashskill.server.entities.dao;

import java.util.List;

import net.skeeks.flashskill.server.entities.CardEntity;
import net.skeeks.flashskill.server.entities.CardFileEntity;
import net.skeeks.flashskill.server.entities.SectionCountResult;
import net.skeeks.flashskill.server.entities.UserEntity;

/**
 * DataAccessObject for CardFileEntity
 * Contains various methods to load/store/delete CardFiles.
 * Abstraction between 3th and 4th Tier. 
 */
public interface ICardFileDAO {

	List<CardFileEntity> loadAllByUser(UserEntity user);

	void deleteById(String cardFileId);

	void store(CardFileEntity cardFile);

	CardFileEntity loadById(String id);

	void deleteAllofUser(UserEntity user);

	List<SectionCountResult> loadGroupBySections(String cardFileId);

	List<CardEntity> loadTop5MissedCards(String cardFileId);

	List<CardEntity> loadTop5KnownCards(String cardFileId);

}
