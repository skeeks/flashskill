package net.skeeks.flashskill.server.mongodb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.google.inject.Singleton;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import net.skeeks.flashskill.server.entities.IEntity;

/**
 * is the implementing class for IDatabaseService. Contains operation to query/update/create/delete entities from the DB. uses Morphia ODM
 * 
 * @author ske
 *
 */
@Singleton
public class DatabaseService implements IDatabaseService {

	private Morphia m_morphia;
	private Datastore m_datastore;
	private MongoClient m_mongoClient;

	private String m_databaseHost;
	private int m_databasePort;
	private String m_databaseName;
	private String m_databaseUser;
	private String m_databasePassword;

	public DatabaseService(String host, int port, String dbname, String dbUser, String dbPassword) {
		m_databaseHost = host;
		m_databasePort = port;
		m_databaseName = dbname;
		m_databaseUser = dbUser;
		m_databasePassword = dbPassword;
	}

	/**
	 * ensures that this service has a connection to the DB. If not, a connection is established
	 */
	protected void ensureConnected() {
		if (m_datastore == null) {
			execConnect();
		}
	}

	protected void execConnect() {
		m_morphia = execCreateMorphia();
		execInitMorphia();
		m_mongoClient = execCreateMongoClient();
		m_datastore = execCreateDatastore();
		execInitDatastore();
	}

	/**
	 * create the MongoClient (with credentials)
	 */
	protected MongoClient execCreateMongoClient() {
		MongoCredential credential = MongoCredential.createCredential(getDatabaseUser(), getDatabaseName(), getDatabasePassword().toCharArray());
		return new MongoClient(new ServerAddress(getDatabaseHost(), getDatabasePort()), Arrays.asList(credential)); // create new mongo connection
	}

	protected Morphia execCreateMorphia() {
		return new Morphia();
	}

	protected Datastore execCreateDatastore() {
		// we need the "advanced" datastore for setting dynamically the
		// collection name.
		return m_morphia.createDatastore(m_mongoClient, getDatabaseName());
	}

	protected void execInitMorphia() {
		m_morphia.mapPackage("net.skeeks.flashskill.server.entities"); // all our database entites live in this package
	}

	protected void execInitDatastore() {
		getDatastore().ensureIndexes(); // ensure indexes on Database
		getDatastore().ensureCaps();
	}

	@Override
	public Datastore getDatastore() {
		ensureConnected();
		return m_datastore;
	}

	@Override
	public Morphia getMorphia() {
		if (m_morphia == null) {
			ensureConnected();
		}
		return m_morphia;
	}

	protected MongoClient getMongoClient() {
		return m_mongoClient;
	}

	public String getDatabaseName() {
		return m_databaseName;
	}

	public String getDatabaseHost() {
		return m_databaseHost;
	}

	protected String getDatabasePassword() {
		return m_databasePassword;
	}

	public String getDatabaseUser() {
		return m_databaseUser;
	}

	public int getDatabasePort() {
		return m_databasePort;
	}

	/**
	 * Store an entity into the database
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public void store(IEntity entity) throws DatabaseException {
		ensureConnected();
		try {
			Key<IEntity> key = getDatastore().save(entity);
			entity.setId((ObjectId) key.getId());
		} catch (Exception e) {
			throw new DatabaseException("could not update entity", e);
		}
	}

	/**
	 * Delete an entity from the database
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public void delete(IEntity entity) throws DatabaseException {
		ensureConnected();
		if (entity == null) {
			throw new DatabaseException("cannot delete 'null' from database");
		}
		try {
			getDatastore().delete(entity.getClass(), entity);
		} catch (Exception e) {
			throw new DatabaseException("could not delete entity", e);
		}
	}

	/**
	 * create a query for an entity
	 */
	@Override
	public <T extends IEntity> Query<T> query(Class<T> entity) {
		ensureConnected();
		return getDatastore().createQuery(entity);
	}

	/**
	 * Execute a query against the Database an return the found entities
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public <T extends IEntity> List<T> find(Query<T> query) throws DatabaseException {
		ensureConnected();
		if (query == null) {
			throw new DatabaseException("no query issued");
		}

		List<T> results = query.asList();
		return results == null ? new ArrayList<T>() : results;
	}

	/**
	 * Delete an entity by its ID (ID passed a string)
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public <T extends IEntity> void delete(Class<T> entityType, String id) throws DatabaseException {
		this.delete(entityType, new ObjectId(id));
	}

	/**
	 * Delete an entity by its ID (ID passed as ObjectId)
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public <T extends IEntity> void delete(Class<T> entityType, ObjectId id) throws DatabaseException {
		try {
			delete(getDatastore().createQuery(entityType).field("_id").equal(id));
		} catch (Exception e) {
			throw new DatabaseException("could not delete entity", e);
		}
	}

	/**
	 * Delete records from the DB matched by a query
	 * 
	 * @throws DatabaseException
	 */
	@Override
	public <T extends IEntity> void delete(Query<T> query) throws DatabaseException {
		try {
			getDatastore().delete(query);
		} catch (Exception e) {
			throw new DatabaseException("could not delete entity", e);
		}
	}
}
