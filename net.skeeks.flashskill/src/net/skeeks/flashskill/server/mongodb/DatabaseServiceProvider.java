package net.skeeks.flashskill.server.mongodb;

import com.google.inject.Provider;

import net.skeeks.flashskill.shared.config.CONFIG;

/**
 * Guice Provider for create IDatabaseService instances.
 * @author ske
 *
 */
public class DatabaseServiceProvider implements Provider<IDatabaseService> {
	/**
	 * Create a new DatabaseService, pass settings from CONFIG as connection credentials
	 */
	@Override
	public IDatabaseService get() {
		return new DatabaseService(CONFIG.get("database.hostname"), Integer.valueOf(CONFIG.get("database.port")),
				CONFIG.get("database.dbname"), CONFIG.get("database.username"), CONFIG.get("database.password"));
	}
}