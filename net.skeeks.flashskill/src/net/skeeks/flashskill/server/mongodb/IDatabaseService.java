package net.skeeks.flashskill.server.mongodb;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import net.skeeks.flashskill.server.entities.IEntity;

public interface IDatabaseService {
	abstract Datastore getDatastore();

	abstract Morphia getMorphia();

	abstract void store(IEntity entity) throws DatabaseException;

	abstract void delete(IEntity entity) throws DatabaseException;

	abstract <T extends IEntity> Query<T> query(Class<T> entity) throws DatabaseException;

	abstract <T extends IEntity> List<T> find(Query<T> query) throws DatabaseException;

	abstract <T extends IEntity> void delete(Class<T> entityType, String id) throws DatabaseException;

	abstract <T extends IEntity> void delete(Class<T> entityType, ObjectId id) throws DatabaseException;

	abstract <T extends IEntity> void delete(Query<T> query) throws DatabaseException;

}
