package net.skeeks.flashskill.server.mongodb;

/**
 * Thrown when an exception in a database operation occurs
 * @author skeeks
 */
class DatabaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DatabaseException() {
	}

	public DatabaseException(String message) {
		super(message);
	}

	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}