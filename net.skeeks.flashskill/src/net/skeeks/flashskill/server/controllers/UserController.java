package net.skeeks.flashskill.server.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.lang3.StringUtils;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.LOG;
import net.skeeks.flashskill.client.common.UserException;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.server.entities.dao.ICardFileDAO;
import net.skeeks.flashskill.server.entities.dao.IUserDAO;
import net.skeeks.flashskill.server.icontrollers.IUserController;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.UserModel;

public class UserController implements IUserController {

	/**
	 * Loads an user. Admin permission required
	 */
	@Override
	public UserModel load(String userId) {
		if (!SessionManager.get().getUser().isAdmin()) { // SecurityCheck
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuf�hren!");
		}
		UserEntity userEntity = IOC.get(IUserDAO.class).loadById(userId);
		if (userEntity == null) {
			return null;
		}
		return toUserModel(userEntity);
	}

	/**
	 * Creates the given user in the database. Admin permission required
	 */
	@Override
	public UserModel create(UserModel user) {
		if (!SessionManager.get().getUser().isAdmin()) { // SecurityCheck
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuf�hren!");
		}

		// check if the username already exists.
		UserEntity userWithUsername = IOC.get(IUserDAO.class).loadByUsername(user.getUsername());
		if (userWithUsername != null) {
			throw new UserException("Ein Benutzer mit dem Benutzernamen '" + user.getUsername() + "' existiert bereits. ");
		}

		if (StringUtils.length(user.getPassword()) < 3) { // password needs to be 3 characters long at least
			throw new UserException("Das Passwort muss mindestens 3 Zeichen lang sein.");
		}
		UserEntity entity = new UserEntity();
		entity.setAdmin(user.isAdmin());
		entity.setUsername(user.getUsername());
		String newSalt = generateSalt();
		// hash the password
		String newHash = Crypt.crypt(user.getPassword(), newSalt);
		entity.setSalt(newSalt); // salt is also stored as separate field
		entity.setPassword(newHash);

		IOC.get(IUserDAO.class).store(entity);
		return toUserModel(entity);
	}

	/**
	 * Creates the given user in the database. Admin permission required
	 */
	@Override
	public UserModel register(UserModel user) {
		// check if the username already exists.
		UserEntity userWithUsername = IOC.get(IUserDAO.class).loadByUsername(user.getUsername());
		if (userWithUsername != null) {
			throw new UserException("Ein Benutzer mit dem Benutzernamen '" + user.getUsername() + "' existiert bereits. ");
		}

		if (StringUtils.length(user.getPassword()) < 3) { // password needs to be 3 characters long at least
			throw new UserException("Das Passwort muss mindestens 3 Zeichen lang sein.");
		}
		UserEntity entity = new UserEntity();
		entity.setAdmin(false);
		entity.setUsername(user.getUsername());
		String newSalt = generateSalt();
		String newHash = Crypt.crypt(user.getPassword(), newSalt);
		entity.setSalt(newSalt);
		entity.setPassword(newHash);

		IOC.get(IUserDAO.class).store(entity);
		return toUserModel(entity);
	}

	/**
	 * Deletes the user with the given id (and all of it's cardfiles). Admin permission required
	 */
	@Override
	public void delete(String userId) {
		if (!SessionManager.get().getUser().isAdmin()) { // SecurityCheck
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuf�hren!");
		}
		UserEntity user = IOC.get(IUserDAO.class).loadById(userId);
		IOC.get(IUserDAO.class).deleteById(userId);
		IOC.get(ICardFileDAO.class).deleteAllofUser(user);
	}

	@Override
	public void changePassword(String newPassword, String oldPassword) {

		UserEntity user = SessionManager.get().getUser();

		String oldHash = Crypt.crypt(oldPassword, user.getSalt());
		if (!user.getPassword().equals(oldHash)) {
			throw new UserException("Das alte Passwort stimmt nicht!");
		}

		// hash new password, but use the old salt
		String newHash = Crypt.crypt(newPassword, user.getSalt());
		user.setPassword(newHash);

		IOC.get(IUserDAO.class).store(user);

		SessionManager.get().setUser(user);

	}

	/**
	 * Saves the given UserModel to the database. Admin permission required
	 */
	@Override
	public UserModel save(UserModel user) {
		if (!SessionManager.get().getUser().isAdmin()) { // SecurityCheck
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuf�hren!");
		}
		// load existing user object from db.
		UserEntity entity = IOC.get(IUserDAO.class).loadById(user.getId());

		// check if user with same username already exists.
		UserEntity userWithUsername = IOC.get(IUserDAO.class).loadByUsername(user.getUsername());
		if (userWithUsername != null && !userWithUsername.getId().equals(entity.getId())) {
			throw new UserException("Ein Benutzer mit dem Benutzernamen '" + user.getUsername() + "' existiert bereits. ");
		}

		entity.setAdmin(user.isAdmin());
		entity.setUsername(user.getUsername());
		if (user.getPassword() != null) { // if no password is set, we keep the old password
			if (StringUtils.length(user.getPassword()) < 4) {
				throw new UserException("Das Passwort muss mindestens 4 Zeichen lang sein.");
			}
			String newSalt = generateSalt();
			String newHash = Crypt.crypt(user.getPassword(), newSalt); // we hash the password with apache commons Crypt utility.
			entity.setSalt(newSalt);
			entity.setPassword(newHash);
		}

		IOC.get(IUserDAO.class).store(entity);

		return toUserModel(entity);

	}

	/**
	 * Checks, if a user with the username "admin" exists and if not, creates that user. This method ensures, that there is always one user at least.
	 */
	@Override
	public void ensureAdminUserExists() {
		UserEntity admin = IOC.get(IUserDAO.class).loadByUsername("admin");
		if (admin == null) {
			LOG.warn("Admin user does not exist! Creating...");
			admin = new UserEntity();
			admin.setAdmin(true);
			admin.setUsername("admin");
			String newSalt = generateSalt();
			String newHash = Crypt.crypt("adminpw", newSalt);
			admin.setSalt(newSalt);
			admin.setPassword(newHash);
			IOC.get(IUserDAO.class).store(admin);
			LOG.warn("Admin user was created");
		}
	}

	/**
	 * Returns a list of all users.
	 */
	@Override
	public List<UserModel> getUsers() {
		if (!SessionManager.get().getUser().isAdmin()) { // SecurityCheck
			throw new SecurityException("Sie sind nicht berechtigt, diese Daten zu sehen.");
		}
		List<UserEntity> users = IOC.get(IUserDAO.class).loadAll();

		List<UserModel> userModels = new ArrayList<>();
		for (UserEntity user : users) {
			userModels.add(toUserModel(user));
		}

		return userModels;
	}

	/**
	 * Converts an UserEntity into a UserModel.
	 * 
	 * @param entity
	 * @return
	 */
	protected UserModel toUserModel(UserEntity entity) {
		UserModel model = new UserModel();
		if (entity.getId() != null) {
			model.setId(entity.getId().toHexString());
		}
		model.setAdmin(entity.isAdmin());
		model.setUsername(entity.getUsername());
		model.setLastLoginDate(entity.getLastLoginDate());
		return model;
	}

	/**
	 * Generate a salt for the Password (currently static)
	 */
	protected String generateSalt() {
		return "$5$flhaw4eub32h�pofij3";
	}

}
