package net.skeeks.flashskill.server.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.entities.CardEntity;
import net.skeeks.flashskill.server.entities.CardFileEntity;
import net.skeeks.flashskill.server.entities.SectionCountResult;
import net.skeeks.flashskill.server.entities.dao.ICardFileDAO;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.server.mongodb.IDatabaseService;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.CardFileModel;
import net.skeeks.flashskill.shared.models.CardFileStatisticsModel;
import net.skeeks.flashskill.shared.models.CardModel;

public class CardFileController implements ICardFileController {

	@Override
	public CardFileModel load(String cardFileId) {
		CardFileEntity cardFileEntity = IOC.get(ICardFileDAO.class).loadById(cardFileId);
		if (cardFileEntity == null) {
			return null;
		}
		// check if CardFile is in ownership of logged in user
		if (!SessionManager.get().isLoggedInUser(cardFileEntity.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Daten zu sehen!");
		}

		return toCardFileModel(cardFileEntity);
	}

	@Override
	public CardFileModel create(CardFileModel cardFile) {
		// needs no permission
		CardFileEntity entity = new CardFileEntity();
		entity.setName(cardFile.getName());
		entity.setOwner(SessionManager.get().getUser());
		for (CardModel card : cardFile.getCards()) {
			if (StringUtils.isBlank(card.getDefinition()) && StringUtils.isBlank(card.getTerm())) {
				continue;
			}
			CardEntity cardEntity = new CardEntity();
			cardEntity.setId(new ObjectId());
			cardEntity.setDefinition(card.getDefinition());
			cardEntity.setTerm(card.getTerm());
			cardEntity.setSection(0);
			cardEntity.setKnownCount(0L);
			cardEntity.setMissedCount(0L);
			entity.getCards().add(cardEntity);
		}
		IOC.get(ICardFileDAO.class).store(entity);

		return toCardFileModel(entity);
	}

	@Override
	public void delete(String cardFileId) {
		CardFileEntity cardFile = IOC.get(ICardFileDAO.class).loadById(cardFileId);

		// check if CardFile is in ownership of logged in user
		if (!SessionManager.get().isLoggedInUser(cardFile.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuführen!");
		}

		IOC.get(IDatabaseService.class).delete(CardFileEntity.class, cardFile.getId());

	}

	@Override
	public void save(CardFileModel responseBean) {
		CardFileEntity loadedEntity = IOC.get(ICardFileDAO.class).loadById(responseBean.getId());

		// check if CardFile is in ownership of logged in user
		if (!SessionManager.get().isLoggedInUser(loadedEntity.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuführen!");
		}

		loadedEntity.setId(new ObjectId(responseBean.getId()));
		loadedEntity.setName(responseBean.getName());
		List<CardEntity> updatedCards = new ArrayList<>();
		for (CardModel card : responseBean.getCards()) {
			CardEntity entityCard = null;
			if (StringUtils.isBlank(card.getDefinition()) && StringUtils.isBlank(card.getTerm())) {
				continue;
			}
			if (card.getId() != null) {
				entityCard = loadedEntity.getCardById(new ObjectId(card.getId()));
			} else {
				entityCard = new CardEntity();
				entityCard.setId(new ObjectId());
			}
			entityCard.setDefinition(card.getDefinition());
			entityCard.setTerm(card.getTerm());

			updatedCards.add(entityCard);
		}
		loadedEntity.setCards(updatedCards);
		IOC.get(ICardFileDAO.class).store(loadedEntity);
	}

	@Override
	public void reset(String cardFileId) {
		CardFileEntity cardFile = IOC.get(ICardFileDAO.class).loadById(cardFileId);
		// check if CardFile is in ownership of logged in user
		if (SessionManager.get().isLoggedInUser(cardFile.getOwner())) {
			cardFile.setLastStudied(null);
			for (CardEntity entity : cardFile.getCards()) {
				entity.setMissedCount(0L);
				entity.setKnownCount(0L);
				entity.setSection(0);
			}
			cardFile.setLearnRound(null);
			IOC.get(ICardFileDAO.class).store(cardFile);
		} else {
			throw new SecurityException("Sie sind nicht berechtigt, diese Aktion auszuführen!");
		}
	}

	@Override
	public List<CardFileModel> getCardFiles() {
		List<CardFileEntity> cardFiles = IOC.get(ICardFileDAO.class).loadAllByUser(SessionManager.get().getUser());
		List<CardFileModel> cardFileModels = new ArrayList<>();
		for (CardFileEntity cardFile : cardFiles) {
			cardFileModels.add(toCardFileModel(cardFile));
		}

		return cardFileModels;
	}

	@Override
	public CardFileStatisticsModel loadStatistics(String cardFileId) {
		CardFileEntity cardFile = IOC.get(ICardFileDAO.class).loadById(cardFileId);
		// check if CardFile is in ownership of logged in user
		if (!SessionManager.get().isLoggedInUser(cardFile.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Daten zu sehen!");
		}
		CardFileStatisticsModel model = new CardFileStatisticsModel();
		model.setCardfilename(cardFile.getName());
		// diagram
		List<SectionCountResult> sectionCounts = IOC.get(ICardFileDAO.class).loadGroupBySections(cardFileId);
		for (SectionCountResult sectionCount : sectionCounts) {
			model.getSections().getLabels().add((Integer.valueOf(sectionCount.getSection()) + 1) + ". Abteilung");
			model.getSections().getValues().add(sectionCount.getCount());
		}

		// top 5 missed cards
		List<CardEntity> topMissedCards = IOC.get(ICardFileDAO.class).loadTop5MissedCards(cardFileId);
		for (CardEntity topCard : topMissedCards) {
			model.getTopMissedCards().add(toCardModel(topCard));
		}

		// top 5 known cards
		List<CardEntity> topKnownCards = IOC.get(ICardFileDAO.class).loadTop5KnownCards(cardFileId);
		for (CardEntity topCard : topKnownCards) {
			model.getTopKnownCards().add(toCardModel(topCard));
		}
		return model;
	}

	/**
	 * Method to convert a CardFileEntity to a CardFileModel
	 */
	protected CardFileModel toCardFileModel(CardFileEntity entity) {
		CardFileModel model = new CardFileModel();
		model.setId(entity.getId().toHexString());
		model.setName(entity.getName());
		model.setLastStudyDate(entity.getLastStudied());
		for (CardEntity cardEntity : entity.getCards()) {
			model.getCards().add(toCardModel(cardEntity));
		}
		return model;
	}

	/**
	 * Method to Convert a CardEntity to a CardModel
	 */
	protected CardModel toCardModel(CardEntity entity) {
		CardModel model = new CardModel();
		if (entity.getId() != null) {
			model.setId(entity.getId().toHexString());
		}
		model.setDefinition(entity.getDefinition());
		model.setTerm(entity.getTerm());
		model.setKnownCount(entity.getKnownCount());
		model.setSection(entity.getSection());
		model.setMissedCount(entity.getMissedCount());
		return model;
	}
}
