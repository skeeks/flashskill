package net.skeeks.flashskill.server.controllers;

import java.util.Date;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.client.common.UserException;
import net.skeeks.flashskill.server.common.CardFileFinishedException;
import net.skeeks.flashskill.server.entities.CardEntity;
import net.skeeks.flashskill.server.entities.CardFileEntity;
import net.skeeks.flashskill.server.entities.dao.ICardFileDAO;
import net.skeeks.flashskill.server.icontrollers.ICardFileController;
import net.skeeks.flashskill.server.icontrollers.ILearnRoundController;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.CardModel;
import net.skeeks.flashskill.shared.models.LearnRoundRequestModel;
import net.skeeks.flashskill.shared.models.LearnRoundResponseModel;

public class LearnRoundController implements ILearnRoundController {

	/**
	 * Returns the current card, which the user has to learn.
	 */
	@Override
	public CardModel getCurrentCard(String cardFileId) {
		CardFileEntity cardFile = IOC.get(ICardFileDAO.class).loadById(cardFileId);
		if (!SessionManager.get().isLoggedInUser(cardFile.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Daten zu sehen");
		}

		CardEntity card = cardFile.getCurrentLearnCard();
		IOC.get(ICardFileDAO.class).store(cardFile);
		if (card == null) {
			throw new UserException("Diese Kartei hat keine Karten!");
		}
		return toCardModel(card);
	}

	/**
	 * Checks, if the user has guessed the right definition. Known- and Missed-Count will be modifed, as well the section. The next card is returned
	 */
	@Override
	public LearnRoundResponseModel processLearn(LearnRoundRequestModel learnRequestModel) {
		String guess = learnRequestModel.getGuess();

		CardFileEntity cardFile = IOC.get(ICardFileDAO.class).loadById(learnRequestModel.getId());
		if (!SessionManager.get().isLoggedInUser(cardFile.getOwner())) {
			throw new SecurityException("Sie sind nicht berechtigt, diese Daten zu sehen");
		}

		LearnRoundResponseModel response = new LearnRoundResponseModel();
		try {
			CardEntity currentCard = cardFile.getCurrentLearnCard();
			if (currentCard == null) {
				throw new UserException("Die Kartei hat keine Karten");
			}
			boolean correct = checkGuess(guess, currentCard.getDefinition());

			response.setId(cardFile.getId().toHexString());
			if (correct) {
				currentCard.increaseKnownCount();
				currentCard.nextSection();
			} else {
				currentCard.increaseMissedCount();
				currentCard.resetSection();
			}
			cardFile.removeCurrentLearnCard();
			cardFile.setLastStudied(new Date());

			response.setNextCard(toCardModel(cardFile.getCurrentLearnCard())); // after this, the card file needs to be stored.
			response.setSolution(currentCard.getDefinition());
			response.setCorrect(correct);
			IOC.get(ICardFileDAO.class).store(cardFile); // this needs to be
		} catch (CardFileFinishedException e) {
			response.setCardFileFinished(true);
			IOC.get(ICardFileController.class).reset(cardFile.getId().toHexString());
		}
		return response;
	}

	/**
	 * Function which checks, if the guess is the same as the solution. outsource for further modification.
	 */
	protected boolean checkGuess(String guess, String solution) {
		return guess != null && guess.equals(solution);
	}

	protected CardModel toCardModel(CardEntity card) {
		CardModel model = new CardModel();
		model.setId(card.getId().toHexString());
		model.setTerm(card.getTerm());
		model.setMissedCount(card.getMissedCount());
		model.setKnownCount(card.getKnownCount());
		model.setSection(card.getSection());
		return model;
	}
}
