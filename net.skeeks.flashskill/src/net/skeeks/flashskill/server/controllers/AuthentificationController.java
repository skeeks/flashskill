package net.skeeks.flashskill.server.controllers;

import java.util.Date;

import org.apache.commons.codec.digest.Crypt;

import net.skeeks.flashskill.IOC;
import net.skeeks.flashskill.server.entities.UserEntity;
import net.skeeks.flashskill.server.entities.dao.IUserDAO;
import net.skeeks.flashskill.server.icontrollers.IAuthentificationController;
import net.skeeks.flashskill.session.SessionManager;
import net.skeeks.flashskill.shared.models.LoginRequestModel;

/**
 * @see IAuthentificationController
 * @author ske
 *
 */
public class AuthentificationController implements IAuthentificationController {

	/**
	 * Login the user into the application
	 */
	@Override
	public UserEntity login(LoginRequestModel model) {
		UserEntity user = IOC.get(IUserDAO.class).loadByUsername(model.getUsername());
		if (user == null) {
			return null;
		}
		// Use Crypt.crypt from apache commonds coded to hash the password.
		String enteredPassword = Crypt.crypt(model.getPassword(), user.getSalt());

		// check if password from db and from loging form is the same
		if (enteredPassword.equals(user.getPassword())) {
			user.setLastLoginDate(new Date());
			IOC.get(IUserDAO.class).store(user);
			SessionManager.get().setUser(user);
			return user;
		} else {
			return null;
		}
	}

	/**
	 * logout the user
	 */
	@Override
	public void logout() {
		SessionManager.get().setUser(null); // set user on session to null
		SessionManager.unset();
	}
};